
package controllerPackage;

import java.util.Observable;
import java.util.Observer;

import theOtherHatTrick.Date;
import theOtherHatTrick.Extension;
import theOtherHatTrick.FirstStrategy;
import theOtherHatTrick.FirstVariant;
import theOtherHatTrick.Game;
import theOtherHatTrick.Player;
import theOtherHatTrick.SecondStrategy;
import theOtherHatTrick.SecondVariant;
import vue.ConsoleInput;
import vue.SwingView;


/**
 * The Controller accorded to the MVC's concept pattern.
 * @author Ingeance
 *
 */
public class Controller implements Observer{

	private SwingView swingView;
	private Game game;
	private String lastInput;

	
	/**
	 * The constructor of the controller.
	 * @param game {Game} : The actual game playing.
	 */
	public Controller(Game game) {
		this.game = game;

	}

	
	/**
	 * Set the last input entered in ConsoleInput.
	 * @param lastInput {String} : The last input.
	 */
	
	public void setLastInput(String lastInput) {
		this.lastInput = lastInput;
	}

	
	/**
	 * Get the last input entered in ConsoleInput.
	 * @return {String} the last input.
	 */
	
	public synchronized String getLastInput() {
		return this.lastInput;
	}

	
	/**
	 * Set the graphic view.
	 * @param swingView {SwingView} : The graphic view.
	 */
	public void setSwingView(SwingView swingView) {

		this.swingView = swingView;
	}

	
	/**
	 * Set the extension's choice.
	 * @param bool {boolean} The choice.
	 */
	public void controlSetExtension(boolean bool) {
		if (bool) {
			Extension.setExtension();
		}
		else {
			Extension.unsetExtension();
		}

		if(swingView != null) {
			swingView.desactiveChoiceExtensionBTN();
		}

		this.lastInput = "break";
		this.game.wakeUP();
	}

	/**
	 * Set the firstVariant's choice.
	 * @param bool {boolean} The choice.
	 */
	
	public void controlSetFirstVariant(boolean bool) {
		if (bool) {
			FirstVariant.setVariant();
		}
		else {
			FirstVariant.unsetVariant();
		}


		if(swingView != null) {
			swingView.desactiveChoiceVariantsBTN();
		}

		this.lastInput = "break";
		this.game.wakeUP();
	}

	
	/**
	 * Set the secondVariant's choice.
	 * @param bool {boolean} The choice.
	 */
	
	public void controleSetSecondVariant(boolean bool) {
		if (bool) {
			SecondVariant.setVariant();
		}else {
			SecondVariant.unsetVariant();
		}
		if(swingView != null) {
			swingView.desactiveChoiceVariantsBTN();
		}
		this.lastInput = "break";
		this.game.wakeUP();
	}

	/**
	 * Set the firstVariant points's choice.
	 * @param n {int} The choice.
	 */
	
	public void controleSetFirstVariantPoints(int n) {


		FirstVariant.setTheNumberOfPointsRequired(n);

		if(swingView != null) {
			swingView.desactiveFirstVariantPoints();
		}
		this.lastInput = "break";
		this.game.wakeUP();
	}

/**
 * Set the number of players's choice.
 * @param g {Game} : The actual game playing.
 * @param n {int} : The choice.
 */
	
	public void setNumberOfPlayer(Game g, int n) {
		g.setNumberOfplayer(n);
		if(swingView != null) {
			swingView.desactiveChoiceNbrPlayersBTN();
		}

		this.lastInput = "break";
		this.game.wakeUP();
	}

	/**
	 * Set the name of player's choice.
	 * @param g {Game} : The actual game playing.
	 * @param name {String} : The choice.
	 */
	
	public void setNameOfPlayer(Game g, String name) {
		g.setName(name);
		if(swingView != null) {
			swingView.desactiveChoiceNameBTN();
		}

		this.lastInput = "break";
		this.game.wakeUP();
	}

	
	/**
	 * Set the strategy's choice.
	 * @param g {Game} : The actual game playing.
	 * @param strat {int} : The choice.
	 */
	public void controlSetStrategy(Game g, int strat) {
		if (strat == 1) {
			g.setStartegy(new FirstStrategy());
		}else if (strat == 2) {
			g.setStartegy(new SecondStrategy());
		}
		if(swingView != null) {
			swingView.desactiveChoiceStrategyBTN();
		}
		this.lastInput = "break";
		this.game.wakeUP();
	}

	/**
	 * Set the player's choice.
	 * @param p {Player} : The actual player.
	 * @param choice {int} : The choice.
	 */
	
	public void controlerSetChoice(Player p, int choice) {
		p.setChoice(choice);
		this.lastInput = "break";
		this.game.wakeUP();
	}
	/**
	 * Set the player other card's choice.
	 * @param p {Player} : The actual player.
	 * @param choice {int} : The choice.
	 */
	
	
	public void controlerSetChooseTheOtherCard(Player p,  int choice) {
		p.setChooseTheOtherCard(choice);
		this.lastInput = "break";
		this.game.wakeUP();
	}

	
	/**
	 * Set the player choose to perform's choice.
	 * @param p {Player} : The actual player.
	 * @param choice {int} : The choice.
	 */
	
	public void controlerSetChoosePerform(Player p, int choice) {
		p.setChoicePerform(choice);
		this.lastInput = "break";
		this.game.wakeUP();
	}

	/**
	 * Set the player showing choice.
	 * @param p {Player} : The actual player.
	 * @param choice {int} : The choice.
	 */
	
	
	public void controlerSetShow(Player p, int choice) {
		p.setShowTrick(choice);
		this.lastInput = "break";
		this.game.wakeUP();
	}

	/**
	 * Set the seventh prop's choice.
	 * @param p {Player} : The actual player.
	 * @param choice {int} : The choice.
	 */
	
	
	public void controlerSetSeventhPropChoice(Player p, int choice) {
		p.setSeventhProp(choice);
		this.lastInput = "break";
		this.game.wakeUP();
	}

	/**
	 * Set the player card returning choice.
	 * @param p {Player} : The actual player.
	 * @param choice {int} : The choice.
	 */
	
	
	public void controlerSetChoiceCardReturn(Player p, int choice) {
		p.setChoiceCardReturn(choice);
		this.lastInput = "break";
		this.game.wakeUP();
	}

	/**
	 * Set the date's choice.
	 * @param g {Game} : The actual game.
	 * @param day {int} : The day's choice.
	 * @param month {int} : The month's choice.
	 * @param year {int} : The year's choice.
	 */
	
	
	public void controlerSetDate(Game g,int day, int month, int year) {
		g.setDataDate(new Date(day,month,year));
		this.swingView.desactiveChoiceDateBTN();
		this.lastInput = "break";
		this.game.wakeUP();
	}

	
	/**
	 * Update operation of the Observer.
	 * Get the last input sent by the ConsoleInput.
	 * @param o {Observable} : An object which implements Observable.
	 * @param arg {Object} : The argument sent in the notification of the Observable. It's an object sent by ConsoleInput.
	 */
	@Override
	public void update(Observable o, Object arg) {
		if(o instanceof ConsoleInput) {
			this.setLastInput((String) arg);
		}

	}


}



