package theOtherHatTrick;
/** 
 * This is the class for the players,
 * They have a name, a birth date, two prop cards in their hands, a number of point, an id and the trick they have to perform.
 * They can choose the trick to perform, exchange their cards with another player and perform or not the trick.
 * @author Ingeance
 */

import java.util.Observable;

/**
 * The main class for players moves.
 * It represents reals players and Virtual Player extends this for artificial players.
 * It interacts with the game, cards and others players.
 * @author Ingeance
 *
 */
@SuppressWarnings("deprecation")
public class Player extends Observable{

	/* Attributes */

	/**
	 * The name of the player.
	 */
	private String name; 
	/**
	 * Birth date of the player.
	 */
	private Date birthDate;
	/**
	 * Card in the left hand.
	 */
	protected Prop leftCard; 
	/**
	 * Card in the right hand.
	 */
	protected Prop rightCard;
	/**
	 * Number of points.
	 */
	protected int numberOfPoints;
	/**
	 * ID to choose a player.
	 */
	private int id; 
	/**
	 * Player Indice used by the views.
	 */
	private int playerIndice;
	/**
	 * Counter used by the views
	 */
	private int count;
	/**
	 * Used by the views to control interactions.
	 */
	protected Events stateVue;
	/**
	 * Choice of player's card to exchange.
	 */
	private int choice;
	/**
	 * Choice of other player's card to exchange with.
	 */
	private int chooseTheOtherCard;
	//private int seventhProp;
	/**
	 * The trick the player has to perform in this turn.
	 */
	protected TrickCard trickHasToPerform;
	/**
	 * The main game.
	 */
	protected Game game;
	/**
	 * Choice to perform the trick.
	 */
	private int choicePerform;
	/*
	 * Card to display by the views.
	 */
	private String displayCards;
	/**
	 * Others players cards to display by the views.
	 */
	private String[] displayCardsGUI;
	/**
	 * Choice to display the trick after sucess.
	 */
	private int showTrick;
	/*
	 * Choice of which card the player will exchange with the seventh prop.
	 */
	private int seventhPropChoice;
	/*
	 * Choice of which card the player will return after a failure.
	 */
	private int choiceCardReturn;

	/**
	 * The constructor of the player.
	 * @param name {String} : The name of the player.
	 * @param leftCard {Prop} : The card at the left for the player.
	 * @param rightCard {Prop} : The card at the right for the player.
	 * @param birthDate {Date} : The birth date of the player.
	 * @param id {int} : The id of the player.
	 * @param game {Game} : The actual game playing.
	 * @see Prop
	 * @see Game
	 */


	public Player(String name, Prop leftCard, Prop rightCard,  Date birthDate, int id, Game game) {

		this.name = name;
		this.leftCard = leftCard;
		this.rightCard = rightCard;
		this.birthDate = birthDate;
		this.numberOfPoints = 0;
		this.id = id;
		this.trickHasToPerform = null;
		this.game = game;
		this.playerIndice = 0;
		this.count = 0;
		this.displayCardsGUI = new String[4];

	}
	/**
	 * The constructor of the player without attributes.
	 */
	public Player() {

	}

	/* Operations */

	/** 
	 * Exchange a card with a player.
	 * Receives a tab of the two other players.
	 * @param player {Player[]} The array of players.
	 * @see Prop
	 */
	public void exchangeCard(Player[] player) { 

		Prop cardToExchange = new Prop();
		this.count = 0;
		choice = 0;
		chooseTheOtherCard = 0; 


		this.stateVue = Events.ChooseCard;
		setChanged();
		notifyObservers(this.stateVue);
		this.game.pause();


		//Choose the other players card you want to switch with 


		StringBuffer str = new StringBuffer();



		for ( this.playerIndice = 0; playerIndice < player.length; playerIndice++) {

			count++;
			str.append(player[playerIndice].name+"\n");
			if (player[playerIndice].leftCard.getIsReturn()) {
				str.append(count + "/ " + player[playerIndice].getLeftCard() + "\n");
				this.displayCardsGUI[count-1] = new String(player[playerIndice].getLeftCard().getPropName().toString());

			}
			else {
				str.append(count + "/ The card is face down \n");
				this.displayCardsGUI[count-1] = new String("down");
			}

			count++;
			if (player[playerIndice].rightCard.getIsReturn()) {
				str.append(count + "/ " + player[playerIndice].getRightCard() + "\n");
				this.displayCardsGUI[count-1] = new String(player[playerIndice].getRightCard().getPropName().toString());

			}
			else {
				str.append(count + "/ The card is face down \n");
				this.displayCardsGUI[count-1] = new String("down");			
			}

		}

		this.displayCards = str.toString();
		this.stateVue = Events.DisplayPlayersCards;
		setChanged();
		notifyObservers(this.stateVue);


		this.stateVue = Events.ChooseProposedCard;
		setChanged();
		notifyObservers(this.stateVue);
		this.game.pause();


		/* Double switch : one for the choice of your card and the other in order to switch your card with another player */
		switch (choice) {
		case 1:
			cardToExchange = this.leftCard;
			switch (chooseTheOtherCard) {
			case 1:
				this.leftCard = player[0].getLeftCard();
				player[0].setLeftCard(cardToExchange);

				break;
			case 2:
				this.leftCard = player[0].getRightCard();
				player[0].setRightCard(cardToExchange);

				break;
			case 3:
				this.leftCard = player[1].getLeftCard();
				player[1].setLeftCard(cardToExchange);

				break;
			case 4:
				this.leftCard = player[1].getRightCard();
				player[1].setRightCard(cardToExchange);

				break;
			default:
				break;
			}
			break;
		case 2:
			cardToExchange = this.rightCard;
			switch (chooseTheOtherCard) {
			case 1:
				this.rightCard = player[0].getLeftCard();
				player[0].setLeftCard(cardToExchange);

				break;
			case 2:
				this.rightCard = player[0].getRightCard();
				player[0].setRightCard(cardToExchange);

				break;
			case 3:
				this.rightCard = player[1].getLeftCard();
				player[1].setLeftCard(cardToExchange);

				break;
			case 4:
				this.rightCard = player[1].getRightCard();
				player[1].setRightCard(cardToExchange);

				break;
			default:
				break;
			}
			break;
		default:
			break;
		}

		this.stateVue = Events.ChangeCards;
		setChanged();
		notifyObservers(this.stateVue);

	}

	/**
	 * The player can choose the trick he wants to perform.
	 * @param trickDeck {TrickDeck} The trick deck.
	 * @param trickPile {TrickPile} The trick Pile.
	 * @see TrickCard
	 * @see TrickDeck
	 * @see TrickPile
	 */
	public void chooseTrick(TrickDeck trickDeck, TrickPile trickPile) { //Choose the trick to perform

		choicePerform = 0;

		if (trickDeck.getNumberOfCardRemainging() > 0) {

			this.stateVue = Events.ChooseToPerform;
			setChanged();
			notifyObservers(this.stateVue);
			this.game.pause();

		}
		else {
			this.stateVue = Events.DisplayLastTrick;
			setChanged();
			notifyObservers(this.stateVue);

			this.choicePerform = 1;
		}

		switch (this.choicePerform) {
		case 1:
			this.trickHasToPerform = trickPile.getTopCardTrickPile();
			break;
		case 2:
			trickDeck.takeCard();
			this.trickHasToPerform = trickPile.getTopCardTrickPile();
			this.stateVue = Events.DisplayTopPile;
			setChanged();
			notifyObservers(this.stateVue);
			break;
		default:
			break;
		}	
	}


	/**
	 * This method returns true if the player has both the necessary props to perform the trick.
	 * @see Prop
	 * @see TrickCard
	 * @param prop
	 * @param prop2
	 * @return test - a boolean.
	 */
	protected boolean contains(Prop prop, Prop prop2) {
		boolean test = false;
		int match1 = -1, match2 = -1;

		for (int k = 0; k < this.trickHasToPerform.getNeedPropOne().length; k++) {
			if (prop.equals(this.trickHasToPerform.getNeedPropOne(k))) {

				match1 = k;
			}	

		}

		for ( int j = 0; j < this.trickHasToPerform.getNeedPropTwo().length; j++) {
			if (prop.equals(this.trickHasToPerform.getNeedPropTwo(j))) {

				match1 = j+2;
			}	

		}

		for ( int j = 0; j < this.trickHasToPerform.getNeedPropTwo().length; j++) {
			if (prop2.equals(this.trickHasToPerform.getNeedPropTwo(j))) {

				match2 = j+2;
			}	

		}

		for (int k = 0; k < this.trickHasToPerform.getNeedPropOne().length; k++) {
			if (prop2.equals(this.trickHasToPerform.getNeedPropOne(k))) {

				match2 = k;
			}	

		}
		/* on va tester le prop 1 d'abord dans la partie gauche du trick puis dans la partie droite 
			si le prop match dans la partie gauche, il aura une valeur comprise entre 0 et  1 si il match dans la partie droite entre 2 et 3
			on test le prop 2 dans l'autre sens, ainsi si les deux prop matchent sur la meme partie du trick ils auront la meme valeur et ainsi on pourra voir que c'est un faux positif que d'avoir les deux bonnes cartes
		 */

		if ((match1 != match2) && ((match1 <= 1 && match1 >=0 && match2 >= 2) || (match2 <= 1 && match2 >=0 && match1 >= 2))) { 

			test = true;
		}

		return test;
	}

	/**
	 * The player can choose whether or not to perform the trick if he can, otherwise he chooses the card he wants to return.
	 * @see TrickPile
	 * @param trickPile {TrickPile} The trick pile.
	 */
	public void performTrick(TrickPile trickPile) { //Decide to perform the trick


		choice = 0;

		if (this.contains(this.leftCard, this.rightCard))
		{
			this.stateVue = Events.ShowSucess;
			setChanged();
			notifyObservers(this.stateVue);
			this.game.pause();


			switch (showTrick) {
			case 1:

				this.numberOfPoints += trickPile.getTopCardTrickPile().getPoint();
				this.stateVue = Events.ShowPoints;
				setChanged();
				notifyObservers(this.stateVue);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				this.game.getTrickDeck().getTrickPile().takeCard();
				if (this.game.getTrickDeck().getTrickPile().getNumberOfCards() == 0)
				{
					this.game.getTrickDeck().takeCard();
				}
				this.stateVue = Events.DisplayTopPile;
				setChanged();
				notifyObservers(this.stateVue);
				this.stateVue = Events.ExchangeSeventhProp;
				setChanged();
				notifyObservers(this.stateVue);
				this.game.pause();


				if (this.leftCard.getIsReturn()) {
					this.leftCard.returnTheCard();
				}
				if (this.rightCard.getIsReturn()) {
					this.rightCard.returnTheCard();
				}
				Prop interProp = new Prop();
				interProp = game.getSeventhProp();
				switch (seventhPropChoice) {
				case 1:
					game.setSeventhProp(this.leftCard);
					this.leftCard = interProp;
					break;
				case 2:
					game.setSeventhProp(this.rightCard);
					this.rightCard = interProp;
					break;
				default:
					break;
				}

				break;

			default:
				break;
			}
			this.stateVue = Events.ChangeCards;
			setChanged();
			notifyObservers(this.stateVue);

		}
		else {


			this.stateVue = Events.TrickFailure;
			setChanged();
			notifyObservers(this.stateVue);

			if (this.leftCard.getIsReturn() && this.rightCard.getIsReturn()) {
				choiceCardReturn = 3;
			}
			else if (this.leftCard.getIsReturn() && !(this.rightCard.getIsReturn())) {
				choiceCardReturn = 4;
			}
			else if (!(this.leftCard.getIsReturn()) && this.rightCard.getIsReturn()) {
				choiceCardReturn = 5;
			}
			else {
				this.stateVue = Events.ReturnCard;
				setChanged();
				notifyObservers(this.stateVue);
				this.game.pause();

			}
			switch (choiceCardReturn) {
			case 1:
				this.leftCard.returnTheCard();
				break;
			case 2:
				this.rightCard.returnTheCard();
				break;
			case 4:
				this.rightCard.returnTheCard();
				break;
			case 5:
				this.leftCard.returnTheCard();
				break;
			default:
				this.stateVue = Events.CardsReturned;
				setChanged();
				notifyObservers(this.stateVue);
				break;
			}

		}

	}

	/* Getter */

	/**
	 * Get the birth date of the player.
	 * @return {Date} : The birth date of the player.
	 */
	public Date getBirthDate() {
		return this.birthDate;

	}
	/*
	 * Get the name of the player.
	 * @return {String} : The name of the player.
	 */
	public String getName() {
		return this.name;
	}
	/**
	 * Get the left card of the player.
	 * @return {Prop} : The left card of the player.
	 */
	public Prop getLeftCard() {
		return this.leftCard;
	}
	/**
	 * Get the right card of the player.
	 * @return {Prop} : The right card of the player.
	 */
	public Prop getRightCard() {
		return this.rightCard;
	}
	/**
	 * Get the number of points of the player.
	 * @return {int} : The number of points of the player.
	 */
	public int getNumberOfPoint() { //Get the number of points
		return this.numberOfPoints;
	}
	/**
	 * Get the Id of the player.
	 * @return {int} : The Id of the player.
	 */
	public int getId() { //Get the Id
		return this.id;
	}
	/**
	 * Get the player indice.
	 * @return {int} : The player indice.
	 */
	public int getI() {
		return this.playerIndice;
	}
	/**
	 * Get the player counter.
	 * @return {int} : The player counter.
	 */
	public int getCount() {
		return this.count;
	}
	/**
	 * Get the state view for the views.
	 * @return {Evenements} : The state view.
	 */
	public Events getStateVue() {
		return this.stateVue;
	}
	/**
	 * Get the card to display.
	 * @return {String} : The card to display.
	 */
	public String getDisplayCards() {

		return this.displayCards;
	}

	/**
	 * Get the trick to perform.
	 * @return {TrickCard} : The trick to perform.
	 */
	public TrickCard getTrickHasToPerform() {
		return trickHasToPerform;
	}
	/**
	 * Get the cards to display.
	 * @return {String[]} : The cards to display.
	 */
	public String[] getDisplayCardsGUI() {
		return displayCardsGUI;
	}
	/**
	 * Set the left card of the player.
	 * @param leftCard {Prop} : The left card.
	 */
	public void setLeftCard(Prop leftCard) {
		this.leftCard = leftCard;
	}
	/**
	 * Set the right card of the player.
	 * @param rightCard {Prop} : The right card.
	 */
	public void setRightCard(Prop rightCard) {
		this.rightCard = rightCard;
	}
	/**
	 * Set the choice of the card.
	 * @param choice {int} : The choice of the card.
	 */
	public void setChoice(int choice) {
		this.choice = choice;
	}
	/**
	 * Set the choice of the other card.
	 * @param c {int} : The choice of the other card.
	 */
	public void setChooseTheOtherCard(int c) {
		this.chooseTheOtherCard = c;
	}
	/**
	 * Get the string of the cards of the player.
	 * @return {String} : The string of the cards of the player.
	 */
	public String toString() {
		return "Left Card : " + this.leftCard + ", Return : " + this.leftCard.getIsReturn() + " " + "Right Card : " + this.rightCard + ", Return : " + this.rightCard.getIsReturn();
	}
	
	/**
	 * Set the choice of perform for the player.
	 * @param choiceP {int} : The choice of perform for the player.
	 */
	public void setChoicePerform(int choiceP) {
		this.choicePerform = choiceP;
	}
	
	/**
	 * Set the choice of show trick for the player.
	 * @param showTrick {int} : The choice of show trick for the player.
	 */
	public void setShowTrick(int showTrick) {
		this.showTrick = showTrick;
	}
	/**
	 * Set the choice of card to change with the seventh prop for the player.
	 * @param choiceSeventh {int} : The choice of card to change with the seventh prop for the player.
	 */
	public void setSeventhProp (int choiceSeventh) {

		this.seventhPropChoice = choiceSeventh;
	}
	/**
	 * Set the choice of card to return for the player.
	 * @param choiceCard {int} : The choice of card to return for the player.
	 */
	public void setChoiceCardReturn(int choiceCard) {
		this.choiceCardReturn = choiceCard;
	}


}
