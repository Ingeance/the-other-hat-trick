package theOtherHatTrick;

import java.util.LinkedList;
import java.util.Observable;

/**
 * The class which represents the pile of cards which is the deck where the player will put his card after taken it on the Trick Deck.
 * @author Ingeance
 *
 */

@SuppressWarnings("deprecation")
public class TrickPile extends Observable{

	/* Attribute */

	private LinkedList<TrickCard> trickPile; // The pile of trick cards
	//private int numberOfCards;

	/* Operations */

	/**
	 * The constructor of the class Trick Pile.
	 */
	public TrickPile() {
		this.trickPile = new LinkedList<TrickCard>();
		//this.numberOfCards = 0;

	}

	/**
	 * The operations that allows the object of Trick Deck to put the card on the Trick Pile (action that can be repeated twice during the same turn).
	 * @param card : the card put on the top of the pile.
	 */
	public void addCard (TrickCard card) { // Add a trick card to the pile
		this.trickPile.add(card);
		setChanged();
		notifyObservers(this);



	}

	/**
	 * The operation that allows the player to take the trick card if he managed the trick.
	 * @return TrickCard : The trick card removed from the zzpile that will collected by the player.
	 */
	public TrickCard takeCard() { // Remove a trick card to the pile
		TrickCard card = this.trickPile.pollLast();
		setChanged();
		notifyObservers(this);

		return card;


	}

	/**
	 * Getter getTopCardTrickPile
	 * @return getTopCardTrickPile : The card at the top of the pile.
	 */
	public TrickCard getTopCardTrickPile() {

		return this.trickPile.get(this.trickPile.size()-1);

	}
	
	/**
	 * Clear the pile.
	 */
	public void clearArray() {
		this.trickPile.clear();
	}

	public String toString() {
		String str = new String();

		for (int i = 0; i < this.trickPile.size(); i++) {
			str += i + "/ " + this.trickPile.get(i) + "\n";
		}
		return str;
	}

	/**
	 * numberOfCards
	 * @return numberOfCards : the number of cards in the trick pile.
	 */
	public int getNumberOfCards() {
		return this.trickPile.size();
	}
	
	/**
	 * Show all the pile in the console.
	 */
	public void showArray() {
		System.out.println("-------------- \n La pile est " + this.trickPile);
	}

}
