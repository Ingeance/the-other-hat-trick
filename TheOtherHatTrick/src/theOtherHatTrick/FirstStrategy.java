package theOtherHatTrick;

import java.util.Random;

/**
 * "Intelligent" strategy.
 * @author Ingeance
 */
public class FirstStrategy implements Strategy{


	public FirstStrategy() {

	}
	/* Operation */

	/**
	 * Choose the card to exchange.
	 * @param virtualPlayer {VirtualPlayer}, the virtual player who wants to make a decision.
	 * @return decision : integer between 1 and 2.
	 */
	public int takeADecisionExchangeHisCard(VirtualPlayer virtualPlayer) { //Virtual Player takes a decision
		int decision = 0;

		int[] verifCard = virtualPlayer.hasCardInHands(virtualPlayer.trickHasToPerform);

		if ((verifCard[0] == 0 && verifCard[1] == 0) || (verifCard[0] > 0 && verifCard[1] > 0)) { // Both or none of the virtual players cards corresponds to the trick card he has to perform
			Random rand = new Random();
			decision = rand.nextInt(1 + 1) + 1;
		}
		else if (verifCard[0] == 0 && verifCard[1] > 0) { 
			decision = 1;
		}
		else if (verifCard[0] > 0 && verifCard[1] == 0) {
			decision = 2;
		}

		return decision;	
	}

	/**
	 * Choose a card from the other players.
	 * @param virtualPlayer {VirtualPlayer}, the virtual player who wants to make a decision.
	 * @param player {Player[]}, the two other players.
	 * @return decision : integer between 1 and 4.
	 */
	public int takeADecisionExchangeWithAnotherPlayer(VirtualPlayer virtualPlayer, Player[] player) { //Virtual Player takes a decision
		int decision = 0, count = 0;
		boolean test = false;


		for (int i = 0; i < player.length; i++) {

			count++;

			if (player[i].leftCard.getIsReturn() && virtualPlayer.leftCard.equals(player[i].leftCard)) {
				return count;
			}


			count++;
			if (player[i].rightCard.getIsReturn() && virtualPlayer.rightCard.equals(player[i].rightCard)) {
				return count;
			}


		}

		Random rand = new Random(); // If none of the flipped cards corresponds, the virtual player choose a random card 

		while (test == false) {
			decision = rand.nextInt(3 + 1) + 1;
			if (player[0].getLeftCard().getIsReturn() && player[0].getRightCard().getIsReturn() && player[1].getLeftCard().getIsReturn() && player[1].getRightCard().getIsReturn()) {
				test = true;
			}
			else {
				switch (decision) {
				case 1:
					if (player[0].getLeftCard().getIsReturn() == false) {
						test = true;
					}
					break;
				case 2:
					if (player[0].getRightCard().getIsReturn() == false) {
						test = true;
					}
					break;
				case 3:
					if (player[1].getLeftCard().getIsReturn() == false) {
						test = true;
					}
					break;
				case 4:
					if (player[1].getRightCard().getIsReturn() == false) {
						test = true;
					}
					break;
				default:
					break;
				}

			}
		}
		return decision;
	}

	/**
	 * Choose the trick to perform.
	 * @param virtualPlayer {VirtualPlayer}, the virtual player who wants to make a decision.
	 * @param trickPile {TrickPile}, the trick pile.
	 * @return decision : integer between 1 and 2.
	 */
	public int takeADecisionChooseTrick(VirtualPlayer virtualPlayer, TrickPile trickPile) {
		int decision = 0;
		int[] verifCard = virtualPlayer.hasCardInHands(trickPile.getTopCardTrickPile());

		if ((verifCard[0] == 0 && verifCard[1] == 0) || (verifCard[0] > 0 && verifCard[1] > 0)) { // Both or none of the virtual players cards corresponds to the trick card on the top of the trick pile
			decision = 2;
		}
		else { 
			decision = 1;
		}

		return decision;
	}


	/**
	 * Choose the card to be returned randomly.
	 * @param virtualPlayer {VirtualPlayer}, the virtual player who wants to make a decision.
	 * @return decision : integer between 1 and 2.
	 */
	public int takeADecisionMalus(VirtualPlayer virtualPlayer) {
		int decision = 0;
		Random rand = new Random();
		decision = rand.nextInt(1 + 1) + 1;
		return decision;
	}


	public String toString() {
		return "First strategy";
	}



}
