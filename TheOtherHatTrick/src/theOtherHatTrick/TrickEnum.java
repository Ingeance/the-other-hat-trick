package theOtherHatTrick;


/**
 * This enumeration represents all the tricks that can make the player.
 * The parameters of the trick enum are :
 * the real name
 * the value of the trick
 * One of the card on the left needed for the trick
 * The second card on the left (if it exists) needed for the trick
 * One of the card on the right needed for the trick
 * The second card on the right (if it exists) needed for the trick
 * @author Ingeance
 *
 */


public enum TrickEnum { 

	TheHungryRabbit ("The Hungry Rabbit","The Infantry Man", 1, PropEnum.Rabbit, PropEnum.TheOtherRabbit, PropEnum.Carrot, PropEnum.Lettuce),
	TheBunchOfCarrots ("The Bunch Of Carrots","The Berserker", 2, PropEnum.Carrot,null,PropEnum.Carrot,null),
	TheVegetablePatch ("The Vegetable Patch", "The Banner Carrier",3,PropEnum.Carrot,null,PropEnum.Lettuce,null),
	TheRabbitThatDidntLikeCarrot ("The Rabbit That Didn't Like Carrot","The Royal Guard", 4, PropEnum.Rabbit, PropEnum.TheOtherRabbit,PropEnum.Lettuce,null),
	ThePairOfRabbits ("The Pair Of Rabbits", "The Sentinel",5, PropEnum.Rabbit,null,PropEnum.TheOtherRabbit,null),
	TheVegetableHatTrick ("The Vegetable Hat Trick","The Horseman", 2, PropEnum.Hat, null, PropEnum.Carrot,PropEnum.Lettuce),
	TheCarrotsHatTrick ("The Carrots Hat Trick", "The Bold Warrior",3, PropEnum.Hat,null,PropEnum.Carrot,null),
	TheSlightlyEasierHatTrick ("The Slightly Easier Hat Trick","The Knight",4,PropEnum.Hat, null,PropEnum.Rabbit,PropEnum.TheOtherRabbit),
	TheHatTrick ("The Hat Trick", "The Legendary Knight",5,PropEnum.Hat, null, PropEnum.Rabbit,null),
	TheOtherHatTrick ("The Other Hat Trick","King Arthur",6,PropEnum.Hat, null, PropEnum.TheOtherRabbit,null),

	// EXTENSION Carrot = Axe, Lettuce = Banner, Hat = Horse, Rabbit = Sword, TheOtherRabbit = Excalibur

	TheRomanticKnight("The Romantic Knight","The Romantic Knight", 6, PropEnum.Lettuce,null,PropEnum.Hat,null),
	TheKingSlayer("The King Slayer","The King Slayer",7,PropEnum.Lettuce, null, PropEnum.TheOtherRabbit, null);


	private String name = "";
	private String extensionName ="";
	private int value;
	Prop[] tabCard1 = new Prop[2];
	Prop[] tabCard2 = new Prop[2];
	int sizeTab1;
	int sizeTab2;

	/**
	 * The constructor of TrickEnum
	 * The parameters of the trick enum are :
	 * the real name
	 * the value of the trick
	 * One of the card on the left needed for the trick
	 * The second card on the left (if it exists) needed for the trick
	 * One of the card on the right needed for the trick
	 * The second card on the right (if it exists) needed for the trick
	 *
	 */
	private TrickEnum(String name, String extensionName, int value, PropEnum prop11, PropEnum prop12, PropEnum prop21, PropEnum prop22){

		this.name= name;
		this.value = value;
		this.extensionName = extensionName;
		this.tabCard1[0] = new Prop(prop11);
		if (prop12 != null) {
			this.tabCard1[1] = new Prop(prop12);
			sizeTab1 = 2;
		} else {
			sizeTab1 = 1;
		}
		this.tabCard2[0] = new Prop(prop21);
		if (prop22 != null) {
			this.tabCard2[1] = new Prop(prop22);
			sizeTab2 = 2;
		}else {
			sizeTab2 = 1;
		}

	}

	public String toString() { // To get the real name of the card.
		if(!Extension.isActivated()) {
			return this.name;
		}else {
			return this.extensionName;
		}
	}

	/** 
	 * Getter value
	 * @return value : The number of points given by the trick.
	 */

	public int getValue() {

		return this.value;
	}

	/**
	 * Getter tabCard1
	 * @return tabCard1 : The first array which represents the cards needed on the left.
	 */
	public Prop[] getTab1() {
		return tabCard1;
	}
	/**
	 * Getter tabCard2
	 * @return tabCard2 : The second array which represents the cards needed on the right.
	 */

	public Prop[] getTab2() {
		return tabCard2;

	}

	/**
	 * Getter sizeTab1
	 * @return sizeTab1 : The size of the first array (without the null).
	 */
	public int getSizeTab1() {

		return sizeTab1;
	}
	/**
	 * Getter sizeTab2
	 * @return sizeTab2 : The size of the second array (without the null).
	 */
	public int getSizeTab2() {

		return sizeTab2;
	}


}

