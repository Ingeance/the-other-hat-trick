package theOtherHatTrick;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Observable;

import java.util.Collections;
/**
 * The main class of The Other Hat Trick.
 * All the game is running from this class.
 * All the events are sent to the twice view accorded to the MVC's concept pattern.
 * @author Ingeance
 *
 */

@SuppressWarnings("deprecation")


public class Game extends Observable implements Runnable {
	/* Attributes */

	/**
	 * Number of real players.
	 * */
	private int numberOfPlayers =-1;
	/**
	 * The seventh hidden card.
	 */
	private Prop seventhProp; 
	/**
	 * Stores the tour number.
	 */
	private int numberOfTurn;
	/**
	 * Array with all the card which will be distributed.
	 */
	private LinkedList<Prop> arrayCardDistribution; 
	/**
	 * Stores the player index.
	 */
	private int playerIndex = 0;
	/**
	 * Stores the players.
	 */
	private Player[] playerTurn; 
	/**
	 * Trick deck of the game.
	 */
	private TrickDeck trickDeck;
	/**
	 * Trick pile of the deck.
	 */
	private TrickPile trickPile;
	/**
	 * Stores the youngest player.
	 */
	private int youngestPlayer;



	/**
	 * 
	 */
	private static int endCounter = 0;
	/**
	 * The evenement which will be sent to the view.
	 */
	private Events viewState;
	/**
	 * The winner of the game.
	 */

	private Player winner;
	/**
	 * The second winner ot the game if it exists.
	 */
	private Player winner2;
	/**
	 * The name of a player.
	 */
	private String name;
	/**
	 * The players counter.
	 */
	private int counter;
	/**
	 * The strategy choosen.
	 */
	private Strategy strategy;
	/**
	 * The date of birthay for player.
	 */
	private Date dateD;
	/* Operations */
	
	/**
	 * The constructor of the class.
	 */
	public Game() {
		this.arrayCardDistribution = new LinkedList<Prop>();
		this.numberOfTurn = 0;
	}

	/**
	 * At the beginning of the game, players can choose their game universe : classic or middle-age.
	 */


	public void activeExtension() {

		this.viewState = Events.ExtensionChoice; 
		setChanged();
		notifyObservers(this.viewState);
		this.pause();
	}

	/**
	 * The player can choose to active two variants.
	 */
	public void activeVariants() {

		this.viewState = Events.VariantsChoice; 
		setChanged();
		notifyObservers(this.viewState);
		this.pause();


		if(FirstVariant.isActivated()) {
			this.viewState = Events.FirstVariantChoice; 
			setChanged();
			notifyObservers(this.viewState);
			this.pause();
		}
	}



	/**
	 * Launch a The Other Hat Trick instance.
	 * @see #activeExtension()
	 * @see #activeVariants()
	 * @see #cardsCreation()
	 * @see #newTurn()
	 */
	public void launchGame() { 

		this.viewState = Events.Welcoming;
		setChanged();
		notifyObservers(this.viewState);

		this.activeExtension();
		this.activeVariants();
		this.viewState = Events.NumberOfPlayers;
		setChanged();
		notifyObservers(this.viewState);
		this.pause();

		// cards are created 
		this.arrayCardDistribution = this.cardsCreation();

		// players are created
		this.getPlayersInformations();

		trickDeck = new TrickDeck();
		trickDeck.createTrickDeck();
		this.trickPile = this.trickDeck.getTrickPile();
		// newTurn is lunched after adding observers
		this.viewState = Events.AddObervers;
		setChanged();
		notifyObservers(this.viewState);
		this.newTurn();
	}



	/**
	 * Stops the game and designates a winner.
	 */
	public void stopGame() { 
		this.winner = new Player();
		this.winner2 = new Player();

		winner = this.playerTurn[this.youngestPlayer];

		if (this.trickPile.getTopCardTrickPile().getTrickName().equals(TrickEnum.TheOtherHatTrick)) {
			for ( int i = 0; i<3; i++) {
				if (this.playerTurn[i].getLeftCard().getPropName().equals(PropEnum.Hat) || this.playerTurn[i].getRightCard().getPropName().equals(PropEnum.Hat)) {
					this.playerTurn[i].numberOfPoints -= 3;
				}
				else if (this.playerTurn[i].getLeftCard().getPropName().equals(PropEnum.TheOtherRabbit) || this.playerTurn[i].getRightCard().getPropName().equals(PropEnum.TheOtherRabbit)) {
					this.playerTurn[i].numberOfPoints -= 3;
				}
				else if ((this.playerTurn[i].getLeftCard().getPropName().equals(PropEnum.Hat) || this.playerTurn[i].getRightCard().getPropName().equals(PropEnum.Hat)) && 
						(this.playerTurn[i].getLeftCard().getPropName().equals(PropEnum.TheOtherRabbit) || this.playerTurn[i].getRightCard().getPropName().equals(PropEnum.TheOtherRabbit))) {
					this.playerTurn[i].numberOfPoints -= 6;
				}
			}
		}

		if (this.playerTurn[0].numberOfPoints == this.playerTurn[1].numberOfPoints && this.playerTurn[0].numberOfPoints == this.playerTurn[2].numberOfPoints) {
			this.viewState = Events.Equality;
			setChanged();
			notifyObservers(this.viewState);
		}
		else {
			for (int i = 0; i<3; i++) {
				if (this.playerTurn[i].numberOfPoints > winner.numberOfPoints) {
					winner = this.playerTurn[i];
				}
				else if (this.playerTurn[i].numberOfPoints == winner.numberOfPoints && winner.equals(winner2) == false) {
					winner2 = this.playerTurn[i];
				}
			}

			this.viewState = Events.DisplayWinner;

			if (winner2.numberOfPoints == winner.numberOfPoints && winner.equals(winner2) == false) {
				this.viewState = Events.DisplayTwoWinners;

			}
			setChanged();
			notifyObservers(this.viewState);

		}
		System.exit(0);
	}



	/**
	 * Get information about the players at the beginning of the game. 
	 */
	private void getPlayersInformations() {
		this.playerTurn = new Player[3];
		String[] takenNames = new String[3];
		Date youngest = new Date(1,1,1);


		Date birthDate;

		for( counter = 0; counter<3; counter++) {
			if(counter<this.numberOfPlayers) {
				this.viewState = Events.ChooseName;
				setChanged();
				notifyObservers(this.viewState);
				this.pause();

				if(Arrays.stream(takenNames).anyMatch(name::equals)) {
					if(Arrays.stream(takenNames).anyMatch((name+ " 2")::equals)) {
						name += " 3";
					} else {
						name += " 2";
					}
				}
				takenNames[counter] = name;
				this.viewState = Events.Date;
				setChanged();
				notifyObservers(this.viewState);
				this.pause();

				birthDate = this.dateD;
				this.playerTurn[counter] = new Player(name, this.arrayCardDistribution.poll(), this.arrayCardDistribution.poll(), birthDate, counter+1, this);

			} else {

				//Virtual players and name verification

				if(counter==1) {
					if(!Arrays.stream(takenNames).anyMatch("Alice"::equals)) {
						name = "Alice";
					}else {
						name = "Antoine";
					}
					birthDate = new Date(15,15,1950);

				}else if(counter==2){
					if(!Arrays.stream(takenNames).anyMatch("Layla"::equals)) {
						name = "Layla";
					} else {
						if(!Arrays.stream(takenNames).anyMatch("Emma"::equals)) {
							name = "Emma";
						}else {
							name = "Ryan";
						}

					}
					birthDate = new Date(15,15,1951);
				}else {
					name = "Laura";
					birthDate = new Date(15,15,1952);
				}

				this.viewState = Events.ChooseStrategy;
				setChanged();
				notifyObservers(this.viewState);
				this.pause();
				this.playerTurn[counter] = new VirtualPlayer(name, this.arrayCardDistribution.poll(), this.arrayCardDistribution.poll(), birthDate, counter+1, this , strategy);
			}

			if(birthDate.getYear() > youngest.getYear()) {

				youngest = birthDate;			
				this.youngestPlayer = (counter);

			}
			else if(birthDate.getMonth() > youngest.getMonth() && birthDate.getYear() == youngest.getYear()) {
				youngest = birthDate;
				this.youngestPlayer = (counter);

			}
			else if(birthDate.getDay() > youngest.getDay() && birthDate.getMonth() == youngest.getMonth() && birthDate.getYear() == youngest.getYear()) {
				youngest = birthDate;
				this.youngestPlayer = (counter);
			}

		}
		this.viewState = Events.Youngest;
		setChanged();
		notifyObservers(this.viewState);
		this.playerIndex = this.youngestPlayer;
		this.seventhProp = this.arrayCardDistribution.poll();
	}

	public Player[] getPlayerTurn() {
		return playerTurn;
	}
	/**
	 * Creates and shuffles the prop cards.
	 * @see Prop
	 * @return cards which is a LinkedList of props.
	 */
	public LinkedList<Prop> cardsCreation() {
		LinkedList<Prop> cards = new LinkedList<Prop>();

		for (PropEnum propEnum : PropEnum.values()) {
			for(int i=0; i< propEnum.getNumber();i++) {
				cards.add(new Prop(propEnum));
			}
		}

		Collections.shuffle(cards);

		return cards;

	}



	/**
	 * Recursive method.
	 * Launches a new game turn until the end of the game.
	 * @see #launchGame()
	 * @see #stopGame()
	 */
	public void newTurn(){ //Player Turn
		Player[] otherPlayers = new Player[2];
		int index = 0;

		for(int i = 0; i<3;i++) {
			if(i != this.playerIndex) {
				otherPlayers[index] = this.playerTurn[i];
				index ++;
			}
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.numberOfTurn++;
		if(this.numberOfTurn > 1) {
			this.viewState = Events.PlayerTurn;
			setChanged();
			notifyObservers(this.viewState);
		}

		this.viewState = Events.BarSeparation;
		setChanged();
		notifyObservers(this.viewState);
		if(this.playerIndex < this.numberOfPlayers) {
			this.viewState = Events.PlayerCards;
			setChanged();
			notifyObservers(this.viewState);
		}
		if (this.trickPile.getNumberOfCards() == 0) {
			this.trickDeck.takeCard();
		}


		this.playerTurn[this.playerIndex].chooseTrick(this.trickDeck, this.trickPile);
		this.playerTurn[this.playerIndex].exchangeCard(otherPlayers);
		this.playerTurn[this.playerIndex].performTrick(this.trickPile);


		this.playerIndex++;
		if(this.playerIndex>2) this.playerIndex = 0;
		if (this.trickPile.getNumberOfCards() == 0) {
			this.trickDeck.takeCard();
		}

		this.viewState = Events.PileDeckCards;
		setChanged();
		notifyObservers(this.viewState);

		if (this.trickDeck.getNumberOfCardRemainging() <= 0  || (this.getHigherPoint() >= FirstVariant.getTheNumberOfPointsRequired() && FirstVariant.isActivated()) || this.numberOfTurn > 299) {

			if (this.trickPile.getTopCardTrickPile().getTrickName().equals(TrickEnum.TheOtherHatTrick) && Game.endCounter < 3) {
				Game.endCounter++;
				this.numberOfTurn++;
				this.newTurn();
			}
			else {
				if(!FirstVariant.isActivated() || this.getHigherPoint() >= FirstVariant.getTheNumberOfPointsRequired() || this.numberOfTurn > 299) {
					this.stopGame();
				}else {
					this.trickDeck.createTrickDeck();
					this.numberOfTurn++;
					this.newTurn();
				}
			}


		}
		else {
			this.numberOfTurn++;
			this.newTurn();
		}


	}
	/**
	 * Get the points of player with most points.
	 * @see Player
	 * @return {int} : The value of max points.
	 */
	public int getHigherPoint() {
		int maxPoints = 0;
		for( int i = 0; i< this.playerTurn.length; i++) {
			if(this.playerTurn[i].getNumberOfPoint() > maxPoints) maxPoints = this.playerTurn[i].getNumberOfPoint();
		}
		return maxPoints;
	}
	/**
	 * Get the number of players.
	 * @return {int} : The number of players.
	 */
	public int getNumberOfPlayer() {
		return this.numberOfPlayers;
	}
	/**
	 * Get the seventh prop.
	 * @see Prop
	 * @return {Prop} : The seventh prop.
	 */
	public Prop getSeventhProp() { //Getter of seventhProp
		return this.seventhProp;
	}
	/**
	 * Get the number of turns.
	 * @return {Prop} : The number of turns.
	 */
	
	public int getNumberOfTurn() { //Getter of numberOfTurn
		return this.numberOfTurn;
	}
	/**
	 * Set the seventh prop.
	 * @param prop {Prop} : The prop which will be set.
	 */

	public void setSeventhProp(Prop prop) {
		this.seventhProp = prop;
	}

	/**
	 * Get the players.
	 * @return {Player[]} : The players.
	 */

	public Player[] getPlayers() {
		return this.playerTurn;
	}

	/**
	 * Get the player index.
	 * @return {int} : The index.
	 */
	
	public int getPlayerIndex() {
		return this.playerIndex;
	}
	
	/**
	 * Get the index of the youngest player.
	 * @return {int} : The index of the youngest player.
	 */
	
	public int getYoungestPlayer() {
		return this.youngestPlayer;
	}

	/**
	 * Get the prop array.
	 * @return {LinkedList} : The prop array.
	 */
	
	public LinkedList<Prop> getPropCards() {
		return this.arrayCardDistribution;
	}
	
	/**
	 * Get the trick deck.
	 * @return {TrickDeck} : The trick deck.
	 */

	public TrickDeck getTrickDeck() {
		return this.trickDeck;
	}
	/**
	 * Get the trick pile.
	 * @return {TrickPile} : The trick pile.
	 */
	
	public TrickPile geTrickPile() {
		return this.trickPile;
	}
	
	/**
	 * Get the winner.
	 * @return {Player} : The winner.
	 */
	
	public Player getWinner() {
		return this.winner;
	}

	/**
	 * Get the second winner.
	 * @return {Player} : The second winner.
	 */
	
	
	public Player getWinner2() {
		return this.winner2;
	}
	
	/**
	 * Get the counter value.
	 * @return {int} : The counter value.
	 */
	
	
	public int getCounter() {
		return this.counter;
	}
	/**
	 * Get the name of the game.
	 * @return {String} : The name of the game.
	 */

	public String getName() {
		return this.name;
	}
	
	/**
	 * Set the number of players.
	 * @param n {Int} : The number of players.
	 */
	
	public void setNumberOfplayer(int n) {
		this.numberOfPlayers = n;
	}
	
	/**
	 * Set the name of the player.
	 * @param name {String} : The name of the player.
	 */
	
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Set the strategy of a virtual player.
	 * @param strat {Strategy} : The strategy of a virtual player.
	 */

	
	public void setStartegy(Strategy strat) {
		this.strategy = strat;
	}

	/**
	 * Set the birth date of a player.
	 * @param d {String} : The birth date of a player.
	 */
	

	public void setDataDate(Date d) {
		this.dateD = d;

	}
	
	/**
	 * Unpause the game.
	 */
	public synchronized void wakeUP() {
		this.notifyAll();
	}

	/**
	 * Pause the game.
	 */
	public synchronized void pause() {
		try {
			this.wait();
		} catch (InterruptedException e) {
			System.out.println("bug");
			e.printStackTrace();

		}
	}

	/**
	 * Launch the game in a new thread.
	 */
	public void run() {
		this.launchGame();


	}
}
