package theOtherHatTrick;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Collections;


/**
 * The class which represents the deck of trick cards. It can be created, mixed and the player can interact with it by taking a card.
 * @author Ingeance
 *
 */
@SuppressWarnings("deprecation")
public class TrickDeck extends Observable{


	private LinkedList<TrickCard> deckTrick; //The deck of trick cards

	private TrickPile trickPile;

	/**
	 * Constructor of TrickDeck, it take no argument.
	 */
	public TrickDeck(){

		this.trickPile = new TrickPile();
		this.deckTrick = new LinkedList<TrickCard>();



	}

	/**
	 * Create the array of trick cards which will represent the deck. After the creation of the deck, it is mixed to avoid having always the same cards in the same order.
	 */
	public void createTrickDeck() { 

		this.deckTrick.clear();
		this.trickPile.clearArray();
		//creation of the deck
		int index = 0;
		for (TrickEnum trickEnum : TrickEnum.values()) {
			if(index<9 || (trickEnum != TrickEnum.TheOtherHatTrick && Extension.isActivated())) {
				this.deckTrick.add(new TrickCard(trickEnum));

			}
			index++;
		}
		Collections.shuffle(this.deckTrick); 
		this.deckTrick.add(new TrickCard(TrickEnum.TheOtherHatTrick));



	}


	/**
	 * Action used by the player to take a card from the deck to put it on the pile.
	 */
	public void takeCard() { //Take a card from the deck


		this.trickPile.addCard(this.deckTrick.poll());

		setChanged();
		notifyObservers(this);
	}

	/**
	 * Getter TrickPile
	 * @return TrickPile : The Object of the class Trick Pile.
	 */
	public TrickPile getTrickPile() {


		return this.trickPile;


	}
	/**
	 * Get all the deck.
	 * @return {LinkedList} The deck.
	 */
	public LinkedList<TrickCard> getListOfTricks() {
		return this.deckTrick;
	}



	/**
	 * Getter numberOfCardsRemaining
	 *  @return numberOfCardsRemaining : The number of cards remaining in the deck.
	 */
	public int getNumberOfCardRemainging() {

		return this.deckTrick.size();
	}
	
	/**
	 * Get the string which will give all the cards of the deck.
	 * @return {String} The string which will give all the cards of the deck.
	 */
	public String toString() {

		StringBuffer str = new StringBuffer();

		for (int i = 0; i < this.deckTrick.size(); i++) {
			str.append( i + "/ " + this.deckTrick.get(i) + "\n");

		}

		return str.toString();
	}


}
