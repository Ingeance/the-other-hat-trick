package theOtherHatTrick;

/**
 * The enumeration which represents all the events sent to the views by the different classes.
 * Events names explains actions performed by the views.
 * @author Ingeance
 *
 */
public enum Events {

	//PlayerEvenements
	ChooseCard,
	DisplayPlayersCards,
	ChooseProposedCard,
	ChooseToPerform,
	DisplayLastTrick,
	DisplayTopPile,
	ShowSucess,
	ExchangeSeventhProp,
	TrickFailure,
	ReturnCard,
	CardsReturned,
	ChangeCards,
	ShowPoints,

	//VirtualPlayerEvenement

	DisplayTrickHasToPerform,
	VirtualPlayerShowSucess,
	VirtualPlayerShowFail,

	//GameEvenements

	Welcoming,
	ExtensionChoice,
	VariantsChoice,
	NumberOfPlayers,
	Equality,
	DisplayWinner,
	DisplayTwoWinners,
	ChooseName,
	ChooseStrategy,
	Youngest,
	PlayerTurn,
	BarSeparation,
	PlayerCards,
	PileDeckCards,
	AddObervers,
	FirstVariantChoice,
	Date
}
