package theOtherHatTrick;

/**
 * Strategy interface for virtuals players.
 * It represents all 4 actions that can perform a virtual player.
 * @author quent
 *
 */
public interface Strategy {
	
	/**
	 * The action to choose card of another player.
	 * @param virtualPlayer {VirtualPlayer} The virtual player that will perform the action.
	 * @param player {Player[]} : All the others players of the game.
	 * @return {int} : The choice.
	 */
	public int takeADecisionExchangeWithAnotherPlayer(VirtualPlayer virtualPlayer, Player[] player); //Virtual Player takes a decision
	/**
	 * The action to choose one of player's cards.
	 * @param virtualPlayer {VirtualPlayer} The virtual player that will perform the action.
	 * @return {int} : The choice.
	 */
	public int takeADecisionExchangeHisCard(VirtualPlayer virtualPlayer);
	/**
	 * The action to choose the trick on the pile or put antoher card on the deck.
	 * @param virtualPlayer {VirtualPlayer} The virtual player that will perform the action.
	 * @param trickPile {TrickPile} : The trick pile of the game.
	 * @return {int} : The choice.
	 */
	public int takeADecisionChooseTrick(VirtualPlayer virtualPlayer, TrickPile trickPile);
	/**
	 * The action to choose the card to return if the player has failed to perform his trick.
	 * @param virtualPlayer {VirtualPlayer} The virtual player that will perform the action.
	 * @return {int} : The choice.
	 */
	public int takeADecisionMalus(VirtualPlayer virtualPlayer);
}
