package theOtherHatTrick;
import java.util.Random;

/**
 * This is the class which represents the Trick Cards which represents the tricks performed by the players .
 * Each Trick has a defined value.
 * Each Trick has a defined name.
 * A Trick Card can have a malus which will takes out points to the player.
 * The tricks needs two cards to be performed.
 * The trick Card can offer two ways to the player to be performed.
 * @author Ingeance
 *
 */


public class TrickCard extends Card {

	/**
	 * Name of the trick card
	 */
	private TrickEnum name;

	/**
	 * Value of the card
	 */
	private int point;

	/**
	 * Active or not the malus
	 */
	private boolean aMalus = false;

	/**
	 * //First card needed
	 */
	private Prop[] needPropOne; 

	/**
	 * Second card needed
	 */
	private Prop[] needPropTwo;


	/** 
	 * Constructor of the class, it takes the enumeration of Trick cards in argument.
	 * @param trick {TrickEnum} The trick enumeration.
	 * */

	public TrickCard(TrickEnum trick) {

		super();
		this.name = trick;
		if (SecondVariant.isActivated() ) {
			Random rand = new Random();
			this.point = rand.nextInt(9 + 1) + 1;
		}
		else {
			this.point = trick.getValue();
		}

		needPropOne = new Prop[trick.getSizeTab1()];
		needPropTwo = new Prop[trick.getSizeTab2()];
		needPropOne[0] = (trick.getTab1())[0];  
		needPropTwo[0] = (trick.getTab2())[0];  
		if(needPropOne.length > 1) needPropOne[1] = (trick.getTab1())[1]; 
		if(needPropTwo.length > 1) needPropTwo[1] = (trick.getTab2())[1];  
		if(trick == TrickEnum.TheOtherHatTrick) this.aMalus = true;


	}

	/**
	 * Active or desactive the malus of the card.
	 */
	public void changeAMalus() { //Change Malus;

		this.aMalus = !this.aMalus;

	}

	/**
	 * Use to display all the properties of the trick card.
	 */
	public  String toString() {
		String tabS;
		String malus;
		if(this.aMalus) {
			malus = ". \n\tThe card has a malus. ";
		}else {
			malus = ". \n\tThe card has no malus. ";
		}
		tabS = "in first hand " + needPropOne[0].toString();
		if(needPropOne.length > 1) {
			tabS = tabS + " or " + needPropOne[1];
		}
		tabS += " and in second hand " +needPropTwo[0].toString();
		if(needPropTwo.length > 1) {
			tabS = tabS + " or " + needPropTwo[1];
		}


		return "The Trick Card is " + this.name.toString() + ". It is worth " + this.point + " points.\n\n\tIt requires " + tabS + malus + "\n";
	}

	/**
	 * name.toString();
	 * @return name.toString() : the name of the card.
	 */
	public String getName() {

		return this.name.toString();
	}

	/**
	 * getTrickName
	 * @return the TrickEnum type of the trick card
	 */
	public TrickEnum getTrickName() {

		return this.name;
	}

	/**
	 * Getter needPropOne
	 * @return needPropOne : the cards needed on the left.
	 */
	public Prop[] getNeedPropOne() {

		return this.needPropOne;

	}
	/**
	 * Getter needPropTwo
	 * @return needPropTwo : the cards needed on the right.
	 */
	public Prop[] getNeedPropTwo() { 

		return this.needPropTwo;

	}

	/**
	 * Getter needPropOne[number]
	 * @param number : the number of the card needed on the left.
	 * @return needPropOne[number] : the card needed on the left.
	 */
	public Prop getNeedPropOne(int number) {

		return this.needPropOne[number];

	}

	/**
	 * Getter needPropTwo[number]
	 * @param number : the number of the card needed on the right.
	 * @return needPropTwo[number] : the card needed on the right.
	 */
	public Prop getNeedPropTwo(int number) { 

		return this.needPropTwo[number];

	}
	/**
	 * Getter point
	 * @return point: the number of points rewarded by the trick.
	 */
	public int getPoint() {
		return this.point;
	}



}
