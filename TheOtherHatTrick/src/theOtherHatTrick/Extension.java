package theOtherHatTrick;

	/**
	 * The static class used to set and unset the variant.
	 * @author Ingeance
	 *
	 */
public class Extension {

	/**
	 * Boolean to know if the extension is activated.
	 */

	private static boolean extensionActivated = false;

	/**
	 * Check if the extension is activated.
	 * @return {boolean} isActivated.
	 */
	
	public static boolean isActivated() {
		return extensionActivated;
	}
	
	/**
	 * Set the extension
	 */

	public static void setExtension() {
		extensionActivated = true;
	}

	/**
	 * Unset the extension
	 */
	public static void unsetExtension() {
		extensionActivated = false;
	}
}
