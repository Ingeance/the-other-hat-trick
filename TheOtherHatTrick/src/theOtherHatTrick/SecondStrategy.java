package theOtherHatTrick;

import java.util.Random;


/**
 * Random strategy.
 * @author Ingeance
 */
public class SecondStrategy implements Strategy {

	/* Operation */

	/**
	 * Choose the card to exchange randomly.
	 * @param virtualPlayer {VirtualPlayer}, the virtual player who wants to make a decision.
	 * @return decision : integer between 1 and 2.
	 */
	public int takeADecisionExchangeHisCard(VirtualPlayer virtualPlayer) { //Virtual Player takes a decision
		int decision = 0;
		Random rand = new Random();
		decision = rand.nextInt(1 + 1) + 1;
		return decision;

	}
	
	/**
	 * Choose a card from the other players randomly.
	 * @param virtualPlayer {VirtualPlayer}, the virtual player who wants to make a decision.
	 * @param player {Player[]}, the two other players.
	 * @return decision : integer between 1 and 4.
	 */
	public int takeADecisionExchangeWithAnotherPlayer(VirtualPlayer virtualPlayer, Player[] player) { //Virtual Player takes a decision
		int decision = 0;
		Random rand = new Random();
		decision = rand.nextInt(4 + 1) + 1; 
		return decision;

	}

	/**
	 * Choose the trick to perform randomly.
	 * @param virtualPlayer {VirtualPlayer}, the virtual player who wants to make a decision.
	 * @param trickPile {TrickPile}, the trick pile.
	 * @return decision : integer between 1 and 2.
	 */
	public int takeADecisionChooseTrick(VirtualPlayer virtualPlayer, TrickPile trickPile) {
		int decision = 0;
		Random rand = new Random();
		decision = rand.nextInt(1 + 1) + 1;
		return decision;
	}

	/**
	 * Choose the card to be returned randomly.
	 * @param virtualPlayer {VirtualPlayer}, the virtual player who wants to make a decision.
	 * @return decision : integer between 1 and 2.
	 */
	public int takeADecisionMalus(VirtualPlayer virtualPlayer) {
		int decision = 0;
		Random rand = new Random();
		decision = rand.nextInt(1 + 1) + 1;
		return decision;
	}


	public String toString() {

		return "Seconde strategie";
	}
}

