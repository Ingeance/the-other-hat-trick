package theOtherHatTrick;


/** 
 * The class which allows us to use dates.
 * @author Ingeance
 *
 */
public class Date {

	private int day;
	private int month;
	private int year;

	/**
	 * The constructor of the class. It takes no argument : the day, the month and the year will be asked every time if it is a real player.
	 */
	public Date() {




	}



	/**
	 * The constructor for the virtual player to allows us to create a fake birth date.
	 * @param day : The day of the date.
	 * @param month : The month of the date.
	 * @param year : The year of the date.
	 */
	public Date(int day, int month, int year) {

		this.day = day; 
		this.month = month;
		this.year = year;

	}

	public String toString() {

		return this.day + "/" + this.month + "/" + this.year;

	}


	public boolean equals(Date d) {
		boolean test = false;

		if (this.day == d.day && this.month == d.month && this.year == d.year) {
			test = true;
		}

		return test;
	}

	/**
	 * day
	 * @return day : The day of the date.
	 */

	public int getDay() {
		return this.day;
	}

	/**
	 * month
	 * @return month : The month of the date.
	 */
	public int getMonth(){
		return this.month;
	}

	/**
	 * year
	 * @return year : The year of the date.
	 */
	public int getYear(){
		return this.year;
	}


}
