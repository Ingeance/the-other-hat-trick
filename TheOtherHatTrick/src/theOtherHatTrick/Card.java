
package theOtherHatTrick;

import java.util.Observable;

/**
 * Class for the cards.
 * @author Ingeance
 *
 */
@SuppressWarnings("deprecation")
public class Card extends Observable{

	public Card() {
		this.isReturn = false;
	}

	/* Attribute*/

	/**
	 * Is the card returns ?
	 */
	private boolean isReturn;
	/**
	 * 
	 * @return isReturn: returns true if the card is returned 
	 */
	public boolean getIsReturn() { 

		return this.isReturn;
	}

	/** Return the card */
	public void returnTheCard() {
		this.isReturn = !this.isReturn;
		setChanged();
		notifyObservers(this);
	}
}
