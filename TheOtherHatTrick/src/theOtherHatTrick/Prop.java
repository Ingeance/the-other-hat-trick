package theOtherHatTrick;

/**
 * Class for the Props.
 * @author Ingeance
 *
 */
public class Prop extends Card {

	/**
	 * Name of the prop in the enumeration.
	 */
	private PropEnum name;

	/**
	 * Constructor.
	 * @param name {PropEnum}, the name of the prop.
	 */
	public Prop(PropEnum name) {
		super();
		this.name = name;

	}
	
	/**
	 * Constructor.
	 */
	public Prop() {
		super();
		this.name = null;
	}

	public String toString() {

		return name.toString();

	}

	/**
	 * Override of the equals method.
	 * Check if two props are equal.
	 * @param prop {Prop} the other prop to test.
	 * @return test {boolean}.
	 */
	public boolean equals(Prop prop) {

		boolean test = false;
		if (this.name == prop.name) {
			test = true;
		}
		return test;
	}

	/**
	 * Getter.
	 * @return name {PropEnum}, the name of the prop in the enumeration.
	 */
	public PropEnum getPropName() {
		return this.name;
	}
}
