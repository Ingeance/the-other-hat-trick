package theOtherHatTrick;
/**
 * The static class used to set and unset first variant.
 * @author Ingeance
 *
 */
public class FirstVariant {

	/**
	 * The number of points required to finish the game.
	 */
	private  static int numberOfPointsRequired;
	/**
	 * Boolean to know if the variant is activated.
	 */
	private static boolean isActivated = false;
	
	/**
	 * Check if the variant is activated.
	 * @return {boolean} isActivated.
	 */
	
	public static boolean isActivated() {
		return isActivated;
	}

	
	/**
	 * Set the variant
	 */

	public static void setVariant() {	
		isActivated = true;
	}

	
	/**
	 * Unset the variant
	 */
	public static void unsetVariant() {
		isActivated = false;
	}
	/**
	 * Set the number of points required to finish the game.
	 * @param n {int} : The number of points required to finish the game.
	 */
	public static void setTheNumberOfPointsRequired(int n) {
		numberOfPointsRequired = n;
	}
	
	/**
	 * Get the number of points required to finish the game.
	 * @return {int} : The number of points required to finish the game.
	 */
	public static int getTheNumberOfPointsRequired() {
		return numberOfPointsRequired;
	}



}
