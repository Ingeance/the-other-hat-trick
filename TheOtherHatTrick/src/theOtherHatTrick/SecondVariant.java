package theOtherHatTrick;

/**
 * The static class used to set and unset second variant.
 * @author Ingeance
 *
 */
public class SecondVariant {


/**
 * Boolean to know if the variant is activated.
 */
	private static boolean isActivated =false;


/**
 * Check if the variant is activated.
 * @return {boolean} isActivated.
 */
	public static boolean isActivated() {
		return isActivated;
	}


	/**
	 * Set the variant
	 */
	public static void setVariant() {

		isActivated = true;

	}
	/**
	 * Unset the variant
	 */
	public static void unsetVariant() {
		isActivated = false;
	}

}



