package theOtherHatTrick;
/**
 * Enumeration of props which will be used to create the cards.
 * It takes two arguments : the real name of the Prop and the number of same cards.
 * @author Ingeance
 *
 */
public enum PropEnum {
	//RAPPEL : Carrot = Axe, Lettuce = Banner, Hat = Horse, Rabbit = Sword, TheOtherRabbit = Excalibur
	Carrot ("Carrot", "Axe", 3),
	Lettuce ("Lettuce", "Banner", 1),
	Rabbit ("Rabbit", "Sword" , 1),
	TheOtherRabbit ("The Other Rabbit", "Excalibur", 1),
	Hat ("Hat", "Horse", 1);

	private String name = "";
	private String extensionName = "";
	private int numberOfProps = 0;
	/**
	 * Constructor of the enumeration
	 * @param name : The name of the Prop with spaces
	 * @param number : The number of same cards.
	 */
	PropEnum(String name, String extensionName, int number){

		this.name= name;
		this.extensionName = extensionName;
		this.numberOfProps = number;
	}



	public String toString() {
		if(!Extension.isActivated()) {
			return this.name;
		}else {
			return this.extensionName;
		}
	}

	/**
	 * Getter numberOfProps
	 * @return numberOfProps : the number of copies of the card.
	 */
	public int getNumber() {
		return this.numberOfProps;
	}


}
