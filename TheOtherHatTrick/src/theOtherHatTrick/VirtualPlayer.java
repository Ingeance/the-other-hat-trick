package theOtherHatTrick;
import java.util.Random;

/**
 * Class for the virtual players.
 * @author Ingeance
 *
 */
public class VirtualPlayer extends Player {
	/* Attribute */

	private Strategy strategy; 
	private TrickPile trickPile;


	/* Constructor */ 

	/**
	 * Constructor of the VirtualPlayer.
	 * @param name {String}, the player's name.
	 * @param leftCard {Prop}, the player's left card.
	 * @param rightCard {Prop}, the player's right card.
	 * @param birthDate {Date}, the birth date of the player.
	 * @param id {int}, the player's ID.
	 * @param game {Game}, to link it with the game object.
	 * @param stratgy {Strategy}, to link it with the strategy object.
	 */
	public VirtualPlayer(String name, Prop leftCard, Prop rightCard,  Date birthDate, int id, Game game, Strategy stratgy) {
		super(name, leftCard, rightCard, birthDate, id, game);
		this.strategy = stratgy;
	}


	/* Operations */
	/**
	 * Override of the Player's method.
	 * @param player {Player[]}, the two other players.
	 */
	public void exchangeCard(Player[] player) { //Exchange a card with a player
		Prop cardToExchange = new Prop();

		int choice, chooseTheOtherCard; 

		choice = this.strategy.takeADecisionExchangeHisCard(this);

		chooseTheOtherCard = this.strategy.takeADecisionExchangeWithAnotherPlayer(this, player);

		

		/** Double switch : one for the choice of your card and the other in order to switch your card with another player */
		switch (choice) {
		case 1:
			cardToExchange = this.leftCard;
			switch (chooseTheOtherCard) {
			case 1:
				this.leftCard = player[0].getLeftCard();
				player[0].setLeftCard(cardToExchange);
				break;
			case 2:
				this.leftCard = player[0].getRightCard();
				player[0].setRightCard(cardToExchange);
				break;
			case 3:
				this.leftCard = player[1].getLeftCard();
				player[1].setLeftCard(cardToExchange);
				break;
			case 4:
				this.leftCard = player[1].getRightCard();
				player[1].setRightCard(cardToExchange);
				break;
			default:
				break;
			}
			break;
		case 2:
			cardToExchange = this.rightCard;
			switch (chooseTheOtherCard) {
			case 1:
				this.rightCard = player[0].getLeftCard();
				player[0].setLeftCard(cardToExchange);
				break;
			case 2:
				this.rightCard = player[0].getRightCard();
				player[0].setRightCard(cardToExchange);
				break;
			case 3:
				this.rightCard = player[1].getLeftCard();
				player[1].setLeftCard(cardToExchange);
				break;
			case 4:
				this.rightCard = player[1].getRightCard();
				player[1].setRightCard(cardToExchange);
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}


	}

	/**
	 * Override of the Player's method.
	 * @param trickDeck {TrickDeck} The trick deck.
	 * @param trickPile {TrickPile} The trick Pile.
	 */
	public void chooseTrick(TrickDeck trickDeck, TrickPile trickPile) { //Choose the trick to perform

		
		int choice = 0;
		this.trickPile = trickPile;
		
		if (trickDeck.getNumberOfCardRemainging() > 0) {
			choice = this.strategy.takeADecisionChooseTrick(this, this.trickPile);
		}
		else {
			choice = 1;
		}
		switch (choice) {
		case 1:
			this.trickHasToPerform = this.trickPile.getTopCardTrickPile();
			break;
			
		case 2:
			trickDeck.takeCard();
			this.stateVue = Events.DisplayTopPile;
			setChanged();
			notifyObservers(this.stateVue);
			this.trickHasToPerform = this.trickPile.getTopCardTrickPile();
			break;
			
		default:
			break;
			
		}

		super.stateVue = Events.DisplayTrickHasToPerform;
		setChanged();
		notifyObservers(super.stateVue);
	}

	
	/**
	 * Override of the Player's method.
	 * @param trickPile {TrickPile} The trick Pile.
	 */
	public void performTrick(TrickPile trickPile) { //Decide to perform the trick

		
		int choice = 0;
		
		if (this.contains(this.getLeftCard(), this.getRightCard()) == true)
		{
			


			this.numberOfPoints += trickPile.getTopCardTrickPile().getPoint();
			this.stateVue = Events.VirtualPlayerShowSucess;
			setChanged();
			notifyObservers(this.stateVue);

			trickPile.takeCard();
			if (trickPile.getNumberOfCards() == 0)
			{
				this.game.getTrickDeck().takeCard();
			}
			this.stateVue = Events.DisplayTopPile;
			setChanged();
			notifyObservers(this.stateVue);
			

			Random rand = new Random();
			int seventhProp = rand.nextInt(2 + 1) + 1;

			if (this.leftCard.getIsReturn()) {
				this.leftCard.returnTheCard();
			}
			if (this.rightCard.getIsReturn()) {
				this.rightCard.returnTheCard();
			}
			Prop interProp = new Prop();
			interProp = game.getSeventhProp();
			switch (seventhProp) {
			case 1:
				game.setSeventhProp(this.leftCard);
				this.leftCard = interProp;
				break;
				
			case 2:
				game.setSeventhProp(this.rightCard);
				this.rightCard = interProp;
				break;
				
			default:
				break;
				
			}



		}
		else {

			this.stateVue = Events.VirtualPlayerShowFail;
			setChanged();
			notifyObservers(this.stateVue);
			if (this.leftCard.getIsReturn() && this.rightCard.getIsReturn()) {
				choice = 3;
			}
			else if (this.leftCard.getIsReturn() && !(this.rightCard.getIsReturn())) {
				choice = 4;
			}
			else if (!(this.leftCard.getIsReturn()) && this.rightCard.getIsReturn()) {
				choice = 5;
			}
			else {
				
				choice = this.strategy.takeADecisionMalus(this);
			}

			switch (choice) {
			case 1:
				this.leftCard.returnTheCard();
				break;
			case 2:
				this.rightCard.returnTheCard();
				break;
			case 4:
				this.rightCard.returnTheCard();
				break;
			case 5:
				this.leftCard.returnTheCard();
				break;
			default:
				break;
			}
		}

	}

	/**
	 * Determines the number of cards corresponding to the trick in the virtual player's hand.
	 * @param trick {TrickCard}. 
	 * @return numberOfCards: the number of cards which match with the trick the virtual player has to perform.
	 */
	public int[] hasCardInHands(TrickCard trick) {
		int numberOfCards[] = {0, 0};

		for (int i = 0; i < trick.getNeedPropOne().length; i++) {
			if (this.leftCard.equals(trick.getNeedPropOne(i))) {

				numberOfCards[0]++;
			}	

		}

		for ( int j = 0; j < trick.getNeedPropTwo().length; j++) {
			if (this.leftCard.equals(trick.getNeedPropTwo(j))) {

				numberOfCards[0]++;
			}	

		}

		for (int i = 0; i < trick.getNeedPropOne().length; i++) {
			if (this.rightCard.equals(trick.getNeedPropOne(i))) {

				numberOfCards[1]++;
			}	

		}

		for ( int j = 0; j < trick.getNeedPropTwo().length; j++) {
			if (this.rightCard.equals(trick.getNeedPropTwo(j))) {

				numberOfCards[1]++;
			}	

		}

		return numberOfCards;
	}


	public TrickPile getTrickPile() {
		return trickPile;
	}


}
