package vue;

import java.awt.EventQueue;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import java.awt.Color;

import theOtherHatTrick.Events;
import theOtherHatTrick.Extension;
import theOtherHatTrick.Game;
import theOtherHatTrick.Player;
import theOtherHatTrick.Prop;
import theOtherHatTrick.TrickCard;
import theOtherHatTrick.TrickDeck;
import theOtherHatTrick.TrickPile;
import theOtherHatTrick.VirtualPlayer;

import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

import java.awt.CardLayout;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

import controllerPackage.Controller;

import javax.swing.JTextField;

/**
 * The graphic view of The Other Hat Trick which will display the game in an interactive window.
 * @author Ingeance
 *
 */
@SuppressWarnings("deprecation")
public class SwingView implements Observer{


	private JFrame frame;
	private JPanelWithBackground panelWithBackground;
	private JLayeredPane layeredPane;

	private JLabel lblIngeance;
	private JLabel lblPlayersTurn;
	private Controller controller;
	private Game game;
	private JLabel lblTrickpile;
	private JLabel lblTrickdeck;
	private JLabel upperTextZone;
	private ButtonInterface btnYes;
	private ButtonInterface btnNo;
	private JTextArea middleTextZone;

	private Font ringBearer;
	private String path;
	private ButtonInterface btnRegulargame;
	private ButtonInterface btnPointsGame;
	private ButtonInterface btnRandomPoints;
	private ButtonInterface btnUseTwice;
	private ButtonInterface buttonPlayer0;
	private ButtonInterface buttonPlayer1;
	private ButtonInterface buttonPlayer2;
	private ButtonInterface buttonPlayer3;
	private JTextField textField;
	private ButtonInterface btnValidate;
	private JTextField dayField;
	private JTextField monthField;
	private JTextField yearField;
	private JLabel dateLabel;
	private ButtonInterface validateDate;
	private JLabel errorText;
	private ButtonInterface btnIntelligent;
	private ButtonInterface btnRandom;
	private JLabel lblPlayername;
	private JLabel lblPlayername_1;
	private JLabel lblPlayername_2;
	private ButtonInterface showSuccessYes;
	private ButtonInterface showSuccessNo;
	private JLabel trickPointLbl;
	private ButtonInterface validateFirstVariantPoints;

	private String state;


	//IMAGES CARTES
	private Image carrot;
	private Image lettuce;
	private Image rabbit;
	private Image otherRabbit;
	private Image hat;
	private Image deck;
	private Image pileBack;
	private Image backBase;
	private Image bunchOfCarrots;
	private Image carrotHatTrick;
	private Image hatTrick;
	private Image hungryRabbit;
	private Image otherHatTrick;
	private Image rabbitThatDidntLikeCarrots;
	private Image slightlyEasierHatTrick;
	private Image thePairOfRabbits;
	private Image theVegetableHatTrick;
	private Image vegetablePatch;
	private Image theRomanticKnight;
	private Image theKingSlayer;

	private ButtonInterface imgTrickPile;
	private ButtonInterface imgTrickDeck;

	private ButtonInterface leftCardJ1;
	private ButtonInterface rightCardJ1;
	private ButtonInterface leftCardJ2;
	private ButtonInterface rightCardJ2;
	private ButtonInterface leftCardJ3;
	private ButtonInterface rightCardJ3;
	private ButtonInterface btnSeventhProp;
	private JTextField firstVariantPointsField;



	/**
	 * Launch the application and the two views.
	 * @see TheOtherHatTrickConsole
	 * @param args {String[]} The main's arguments.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Game g = new Game();
					Controller c = new Controller(g);

					SwingView window = new SwingView(g, c);
					c.setSwingView(window);
					TheOtherHatTrickConsole t = new TheOtherHatTrickConsole(g,c); 

					Thread gameThread = new Thread (g);
					gameThread.start();
				} 
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}



	/**
	 * Create the application.
	 * @param g {Game} : The actual game.
	 * @param c {Controller} : The controller.
	 */
	public SwingView(Game g, Controller c) {
		this.game = g;
		this.controller = c;
		this.path =new File("").getAbsolutePath();
		initialize();
		this.frame.setVisible(true);
		this.game.addObserver(this);
	}



	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.loadFont();
		frame =  	new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);

		frame.setBounds(100, 100, 450, 300);
		frame.setSize(1280, 720);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			panelWithBackground = new JPanelWithBackground(this.path + "//src//Images//background.jpg");
			frame.getContentPane().add(panelWithBackground);
			panelWithBackground.setLayout(new CardLayout(0, 0));


			layeredPane = new JLayeredPane();
			panelWithBackground.add(layeredPane, "name_345420112675189");
			layeredPane.setLayout(null);


			btnSeventhProp = new ButtonInterface("seventh prop");
			btnSeventhProp.setBounds(522, 231, 121, 170);
			layeredPane.add(btnSeventhProp);


			lblPlayersTurn = new JLabel("Player's turn");
			lblPlayersTurn.setFont(new Font("Georgia", Font.PLAIN, 21));
			lblPlayersTurn.setBounds(426, 68, 508, 31);
			layeredPane.add(lblPlayersTurn);


			lblTrickpile = new JLabel("TrickPile");
			lblTrickpile.setBounds(74, 320, 301, 48);
			layeredPane.add(lblTrickpile);


			lblTrickdeck = new JLabel("TrickDeck");
			lblTrickdeck.setBounds(310, 308, 379, 60);
			layeredPane.add(lblTrickdeck);


			upperTextZone = new JLabel("The Other Hat Trick");
			upperTextZone.setHorizontalAlignment(SwingConstants.CENTER);
			upperTextZone.setFont(new Font("Old London", Font.PLAIN, 90));
			upperTextZone.setBounds(200, 6, 833, 143);
			layeredPane.add(upperTextZone);


			middleTextZone = new JTextArea("Do you want to use middle age extension it changes so many things that it is crazy");
			layeredPane.setLayer(middleTextZone, -1);
			middleTextZone.setEditable(false);
			middleTextZone.setWrapStyleWord(true);
			middleTextZone.setLineWrap(true);


			this.middleTextZone.setFont(this.ringBearer.deriveFont(20.0f));
			middleTextZone.setBounds(354, 161, 508, 207);
			middleTextZone.setOpaque(false);

			layeredPane.add(middleTextZone);


			errorText = new JLabel("Error : Please write a number");
			errorText.setBackground(Color.YELLOW);
			errorText.setFont(new Font("Tahoma", Font.PLAIN, 18));
			errorText.setForeground(Color.RED);
			errorText.setBounds(12, 543, 379, 120);
			layeredPane.add(errorText);


			this.choiceExtensionBTN();
			this.choiceVariantsBTN();
			this.choiceNbrPlayersBTN();
			this.choiceNameBTN();
			this.choiceDateBTN();
			this.choiceStrategyBTN();
			this.trickPileBTN();
			this.trickDeckBTN();
			this.playerCardsBTN();
			this.otherPlayersCards();
			this.firstVariantPointsBTN();


			buttonPlayer0.setBounds(111, 357, 169, 120);
			layeredPane.add(buttonPlayer0);


			lblPlayername = new JLabel("PlayerName3");
			lblPlayername.setHorizontalAlignment(SwingConstants.CENTER);
			lblPlayername.setForeground(Color.getHSBColor(((float)358), ((float)0.77), (float)0.43));
			lblPlayername.setFont(this.ringBearer.deriveFont(25.0f));
			lblPlayername.setBounds(386, 445, 365, 31);
			layeredPane.add(lblPlayername);
			this.lblPlayername.setText("");


			lblPlayername_1 = new JLabel("PlayerName1");
			lblPlayername_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblPlayername_1.setForeground(Color.BLACK);
			lblPlayername_1.setFont(this.ringBearer.deriveFont(25.0f));
			lblPlayername_1.setBounds(106, 24, 198, 31);
			layeredPane.add(lblPlayername_1);
			this.lblPlayername_1.setText("");


			lblPlayername_2 = new JLabel("PlayerName2");
			lblPlayername_2.setHorizontalAlignment(SwingConstants.CENTER);
			lblPlayername_2.setForeground(Color.BLACK);
			lblPlayername_2.setFont(this.ringBearer.deriveFont(25.0f));
			lblPlayername_2.setBounds(914, 24, 205, 31);
			layeredPane.add(lblPlayername_2);
			this.lblPlayername_2.setText("");


			showSuccessYes = new ButtonInterface("Yes");
			showSuccessYes.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					controller.controlerSetShow(game.getPlayers()[game.getPlayerIndex()], 1);
				}
			});
			layeredPane.setLayer(showSuccessYes, 4);
			showSuccessYes.setBounds(355, 355, 170, 125);
			layeredPane.add(showSuccessYes);


			showSuccessNo = new ButtonInterface("No");
			showSuccessNo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					showSuccessYes.setVisible(false);
					showSuccessNo.setVisible(false);
					activeGameBTN();
					controller.controlerSetShow(game.getPlayers()[game.getPlayerIndex()], 2);
				}
			});
			layeredPane.setLayer(showSuccessNo, 4);
			showSuccessNo.setBounds(638, 358, 169, 120);
			layeredPane.add(showSuccessNo);


			trickPointLbl = new JLabel("6");
			trickPointLbl.setForeground(Color.getHSBColor(((float)358), ((float)0.77), (float)0.43));
			layeredPane.setLayer(trickPointLbl, 2);
			trickPointLbl.setFont(new Font("Verdana", Font.BOLD, 28));
			trickPointLbl.setBounds(144, 461, 33, 48);
			layeredPane.add(trickPointLbl);


			this.desactiveChoiceVariantsBTN();
			this.desactiveChoiceNbrPlayersBTN();
			this.desactiveChoiceNameBTN();
			this.desactiveChoiceDateBTN();
			this.desactiveChoiceStrategyBTN();
			this.desactiveGameBTN();
			this.desactiveFirstVariantPoints();
			this.errorText.setVisible(false);

	
	}

	/**
	 * Update operation of the Observer.
	 * @see Game
	 * The view will react to all the events according to the MVC's design pattern.
	 * @param o {Observable} : An object which implements Observable.
	 * @param arg {Object} : The argument sent in the notification of the Observable. It's the enumeration Evenements.
	 */

	@Override
	public void update(Observable o, Object arg) {

		if (o instanceof Game) 
		{
			switch (((Events)arg)) {
			case Welcoming:
				this.upperTextZone.setText("The Other Hat Trick");
				this.upperTextZone.setFont(this.ringBearer.deriveFont(70.0f));
				break;

			case ExtensionChoice:
				this.middleTextZone.setVisible(true);
				this.middleTextZone.setText("Do you want to use Middle-Age extension ? \n It changes the universe of the game and add two new trick cards !");
				break;

			case VariantsChoice:
				this.createImageIcons();
				this.leftCardJ1.setIcon(new ImageIcon(this.backBase));
				this.rightCardJ1.setIcon(new ImageIcon(this.backBase));
				this.leftCardJ2.setIcon(new ImageIcon(this.backBase));
				this.rightCardJ2.setIcon(new ImageIcon(this.backBase));
				this.leftCardJ3.setIcon(new ImageIcon(this.backBase));
				this.rightCardJ3.setIcon(new ImageIcon(this.backBase));
				this.imgTrickDeck.setIcon(new ImageIcon(this.deck));
				this.imgTrickPile.setIcon(new ImageIcon(this.pileBack));
				this.btnSeventhProp.setIcon(new ImageIcon(this.backBase));
				this.middleTextZone.setText("Do you want to use variants ? \n  1. Regular game. \n 2. The winner is defined by his number of points, can be used for short or very long games !\n 3. The points of the tricks are given randomly ! \n 4. Active the two variants");
				this.activeChoiceVariantsBTN();
				break;

			case FirstVariantChoice :
				this.activeFirstVariantPoints();
				this.middleTextZone.setText("How many points will be necessary ? (Maximum 50)");
				break;

			case NumberOfPlayers:
				this.middleTextZone.setText("Choose the number of real players");
				this.activeChoiceNbrPlayersBTN();
				break;

			case Equality:
				JOptionPane.showMessageDialog(frame, "Equality !");
				break;

			case DisplayWinner:
				JOptionPane.showMessageDialog(frame, "The winner is : " + this.game.getWinner().getName()+ " with " + this.game.getWinner().getNumberOfPoint() + " points");
				break;

			case DisplayTwoWinners:
				StringBuffer sb = new StringBuffer();
				sb.append("The winner is : " + this.game.getWinner().getName()+ " with " + this.game.getWinner().getNumberOfPoint() + " points\n ");
				sb.append("The second winner is : " + this.game.getWinner2().getName() + " with " + this.game.getWinner2().getNumberOfPoint() + " points");
				JOptionPane.showMessageDialog(frame, sb.toString());
				break;

			case ChooseName:
				this.activeChoiceNameBTN();
				this.middleTextZone.setText("Player " + (this.game.getCounter()+1) + ", Please write your name :\nOnly 10 first letters will be counted.\n 2 letters minimum.");
				break;

			case Date :
				this.middleTextZone.setText("Please enter your birth date");
				this.activeChoiceDateBTN();
				break;

			case ChooseStrategy:
				this.middleTextZone.setText("Please choose the strategy to adopt for " + this.game.getName());
				this.activeChoiceStrategyBTN();
				break;

			case Youngest:
				this.lblPlayersTurn.setText("The youngest player is " + (this.game.getPlayers()[this.game.getYoungestPlayer()].getName()) + ". It's your turn !");
				this.lblPlayername.setText(this.game.getPlayerTurn()[this.game.getYoungestPlayer()].getName());
				this.lblPlayername_1.setText(this.game.getPlayerTurn()[2].getName());
				this.lblPlayername_2.setText(this.game.getPlayerTurn()[1].getName());
				break;

			case AddObervers :
				Player[] players = this.game.getPlayers();
				for (int i = 0; i < players.length; i++)
					players[i].addObserver(this);

				LinkedList<Prop> propCards = this.game.getPropCards();
				Iterator<Prop> itProp = propCards.iterator();
				while (itProp.hasNext()) {
					Prop propCard = itProp.next();
					propCard.addObserver(this);
				}

				TrickDeck trickDeckObs = this.game.getTrickDeck();
				trickDeckObs.addObserver(this);

				Iterator<TrickCard> itTrick = trickDeckObs.getListOfTricks().iterator();
				while (itTrick.hasNext()) {
					TrickCard trickCard = itTrick.next();
					trickCard.addObserver(this);
				}

				TrickPile trickPileObs = this.game.getTrickDeck().getTrickPile();
				trickPileObs.addObserver(this);

				this.activeGameBTN();
				this.upperTextZone.setText("");
				this.middleTextZone.setText("");
				break;

			case PlayerTurn:
				this.lblPlayersTurn.setText("");
				this.middleTextZone.setText("\nIt's " + (this.game.getPlayers()[this.game.getPlayerIndex()].getName()) + "'s turn !");
				break;

			case PlayerCards:
				String leftCard = this.game.getPlayers()[this.game.getPlayerIndex()].getLeftCard().toString();
				String rightCard = this.game.getPlayers()[this.game.getPlayerIndex()].getRightCard().toString();

				this.leftCardJ1.setIcon(new ImageIcon(this.nameToImage(leftCard)));
				this.rightCardJ1.setIcon(new ImageIcon(this.nameToImage(rightCard)));
				break;

			case PileDeckCards:
				this.lblTrickdeck.setText("There are " + this.game.getTrickDeck().getNumberOfCardRemainging() + " card(s) in the deck");
				this.lblTrickpile.setText("There are " + this.game.geTrickPile().getNumberOfCards() + " card(s) in the pile");
				break;

			default:
				break;

			}
		}
		else if (o instanceof Player) 
		{
			switch (((Events)arg)) {
			case ChooseCard:
				middleTextZone.setText("Please choose between your cards");

				this.state = "ChooseMyCard";
				break;

			case DisplayPlayersCards:
				if (this.game.getPlayers()[this.game.getPlayerIndex()].getDisplayCardsGUI()[0].equals("down") == false) {
					this.leftCardJ2.setIcon(new ImageIcon(this.nameToImage(this.game.getPlayers()[this.game.getPlayerIndex()].getDisplayCardsGUI()[0])));
				}
				if (this.game.getPlayers()[this.game.getPlayerIndex()].getDisplayCardsGUI()[1].equals("down") == false) {
					this.rightCardJ2.setIcon(new ImageIcon(this.nameToImage(this.game.getPlayers()[this.game.getPlayerIndex()].getDisplayCardsGUI()[1])));
				}
				if (this.game.getPlayers()[this.game.getPlayerIndex()].getDisplayCardsGUI()[2].equals("down") == false) {
					this.leftCardJ3.setIcon(new ImageIcon(this.nameToImage(this.game.getPlayers()[this.game.getPlayerIndex()].getDisplayCardsGUI()[2])));
				}
				if (this.game.getPlayers()[this.game.getPlayerIndex()].getDisplayCardsGUI()[3].equals("down") == false) {
					this.rightCardJ3.setIcon(new ImageIcon(this.nameToImage(this.game.getPlayers()[this.game.getPlayerIndex()].getDisplayCardsGUI()[3])));
				}
				break;

			case ChooseProposedCard: 
				this.middleTextZone.setText("Please choose a card of one of your opponents");
				this.state = "ChooseOpponentCard";
				break;

			case ChooseToPerform :
				this.state = "ChooseToPerform";
				this.middleTextZone.setText(this.game.getPlayerTurn()[this.game.getPlayerIndex()].getName() + ", \nPlease choose between the available Trick in the pile or an another trick on the deck");
				this.lblPlayername.setText(this.game.getPlayerTurn()[this.game.getPlayerIndex()].getName());

				int index = this.game.getPlayerIndex();

				if (index == 0) {
					this.lblPlayername_1.setText(this.game.getPlayers()[2].getName());
					this.lblPlayername_2.setText(this.game.getPlayers()[1].getName());
				}
				else if (index == 1) {
					this.lblPlayername_1.setText(this.game.getPlayers()[2].getName());
					this.lblPlayername_2.setText(this.game.getPlayers()[0].getName());
				}
				else if (index == 2) {
					this.lblPlayername_1.setText(this.game.getPlayers()[1].getName());
					this.lblPlayername_2.setText(this.game.getPlayers()[0].getName());
				}

				this.imgTrickPile.setIcon(new ImageIcon(this.nameToImage(this.game.getTrickDeck().getTrickPile().getTopCardTrickPile().getName())));
				this.trickPointLbl.setText(this.game.getTrickDeck().getTrickPile().getTopCardTrickPile().getPoint() +"");
				break;

			case DisplayLastTrick :
				this.middleTextZone.setText("There are no more cards available");
				this.imgTrickPile.setIcon(new ImageIcon(this.nameToImage(this.game.getTrickDeck().getTrickPile().getTopCardTrickPile().getName())));
				this.imgTrickDeck.setVisible(false);
				this.trickPointLbl.setText(this.game.getTrickDeck().getTrickPile().getTopCardTrickPile().getPoint() +"");
				break;

			case DisplayTopPile :
				this.imgTrickPile.setIcon(new ImageIcon(this.nameToImage(this.game.getTrickDeck().getTrickPile().getTopCardTrickPile().getName())));
				this.trickPointLbl.setText(this.game.getTrickDeck().getTrickPile().getTopCardTrickPile().getPoint() +"");
				break;

			case ChangeCards :
				this.btnSeventhProp.setIcon(new ImageIcon(this.backBase));
				this.leftCardJ2.setIcon(new ImageIcon(this.backBase));
				this.rightCardJ2.setIcon(new ImageIcon(this.backBase));
				this.leftCardJ3.setIcon(new ImageIcon(this.backBase));
				this.rightCardJ3.setIcon(new ImageIcon(this.backBase));
				String leftCard = this.game.getPlayers()[this.game.getPlayerIndex()].getLeftCard().toString();
				String rightCard = this.game.getPlayers()[this.game.getPlayerIndex()].getRightCard().toString();

				this.leftCardJ1.setIcon(new ImageIcon(this.nameToImage(leftCard)));
				this.rightCardJ1.setIcon(new ImageIcon(this.nameToImage(rightCard)));
				break;

			case ShowSucess :
				this.middleTextZone.setText("You succeded");
				this.middleTextZone.setText("Do you want to show that you have succeeded the trick ?");
				this.desactiveGameBTN();
				this.showSuccessNo.setVisible(true);
				this.showSuccessYes.setVisible(true);
				this.imgTrickPile.setVisible(true);
				this.lblPlayername.setText(this.game.getPlayerTurn()[this.game.getPlayerIndex()].getName() + " has " + (this.game.getPlayerTurn()[this.game.getPlayerIndex()].getNumberOfPoint()) + " points !" );
				break;

			case ShowPoints :
				this.showSuccessNo.setVisible(false);
				this.showSuccessYes.setVisible(false);
				this.activeGameBTN();
				this.middleTextZone.setText("You have " + this.game.getPlayers()[this.game.getPlayerIndex()].getNumberOfPoint() + " points !");
				this.lblPlayername.setText(this.game.getPlayerTurn()[this.game.getPlayerIndex()].getName() + " has " + (this.game.getPlayerTurn()[this.game.getPlayerIndex()].getNumberOfPoint()) + " points !" );
				break;

			case ExchangeSeventhProp:
				state = "ChooseSeventhProp";
				this.btnSeventhProp.setIcon(new ImageIcon(this.nameToImage(this.game.getSeventhProp().toString())));
				this.middleTextZone.setText("\nWhich card do you want to put in the middle ?");
				break;

			case TrickFailure :
				this.middleTextZone.setText("You failed");
				break;

			case ReturnCard :
				this.middleTextZone.setText("You failed \nPlease return one of your cards");
				this.state = "Return Card";
				break;

			case CardsReturned:
				this.middleTextZone.setText("No more malus, your cards are return");
				break;

			case DisplayTrickHasToPerform:
				this.middleTextZone.setText((((VirtualPlayer) this.game.getPlayers()[this.game.getPlayerIndex()]).getTrickPile().getTopCardTrickPile()).getName());
				break;

			case VirtualPlayerShowSucess:
				this.middleTextZone.setText((((VirtualPlayer) this.game.getPlayers()[this.game.getPlayerIndex()]).getName()) + " performed" + (((VirtualPlayer) this.game.getPlayers()[this.game.getPlayerIndex()]).getTrickHasToPerform().getName()) + " with the cards " + (((VirtualPlayer) this.game.getPlayers()[this.game.getPlayerIndex()]).getLeftCard()) + " and " + (((VirtualPlayer) this.game.getPlayers()[this.game.getPlayerIndex()]).getRightCard()));
				break;

			case VirtualPlayerShowFail:
				this.middleTextZone.setText((((VirtualPlayer) this.game.getPlayers()[this.game.getPlayerIndex()]).getName()) + "  failed to perform " + (((VirtualPlayer) this.game.getPlayers()[this.game.getPlayerIndex()]).getTrickHasToPerform().getName()) + " with the cards " + (((VirtualPlayer) this.game.getPlayers()[this.game.getPlayerIndex()]).getLeftCard()) + " and " + (((VirtualPlayer) this.game.getPlayers()[this.game.getPlayerIndex()]).getRightCard()));
				break;

			default:
				break;

			}
		}
	}





	/**
	 * Give an image for a String.
	 * @param name {String} : The name of the image.
	 * @return {Image} : The image accorded to the name.
	 */

	public Image nameToImage(String name) {

		switch(name) {

		//PROPS
		case "Carrot" :
			return this.carrot;
		case "Axe":
			return this.carrot;

		case "Lettuce" :
			return this.lettuce;
		case "Banner":
			return this.lettuce;

		case "Hat" :
			return this.hat;
		case "Horse":
			return this.hat;

		case "The Other Rabbit" :
			return this.otherRabbit;
		case "Excalibur":
			return this.otherRabbit;

		case "Rabbit" :
			return this.rabbit;
		case "Sword":
			return this.rabbit;


			//TRICKS
		case "The Hungry Rabbit" :
			return this.hungryRabbit;
		case "The Infantry Man" :
			return this.hungryRabbit;

		case "The Bunch Of Carrots" :
			return this.bunchOfCarrots;
		case "The Berserker" :
			return this.bunchOfCarrots;

		case "The Vegetable Patch" :
			return this.vegetablePatch;
		case "The Banner Carrier" :
			return this.vegetablePatch;

		case "The Rabbit That Didn't Like Carrot" :
			return this.rabbitThatDidntLikeCarrots;
		case "The Royal Guard" :
			return this.rabbitThatDidntLikeCarrots;

		case "The Pair Of Rabbits" :
			return this.thePairOfRabbits;
		case "The Sentinel" :
			return this.thePairOfRabbits;

		case "The Vegetable Hat Trick" :
			return this.theVegetableHatTrick;
		case "The Horseman" :
			return this.theVegetableHatTrick;

		case "The Carrots Hat Trick" :
			return this.carrotHatTrick;
		case "The Bold Warrior" :
			return this.carrotHatTrick;

		case "The Slightly Easier Hat Trick" :
			return this.slightlyEasierHatTrick;
		case "The Knight" :
			return this.slightlyEasierHatTrick;

		case "The Hat Trick" :
			return this.hatTrick;
		case "The Legendary Knight" :
			return this.hatTrick;

		case "The Other Hat Trick" :
			return this.otherHatTrick;
		case "King Arthur" :
			return this.otherHatTrick;

		case "The Romantic Knight":
			return this.theRomanticKnight;

		case "The King Slayer":
			return this.theKingSlayer;


		default:
			return this.carrot;
		}


	}

	/**
	 * Create all the cards, deck or pile images from the folder Images.
	 */

	public void createImageIcons() {
		try {
			if (Extension.isActivated()) {

				//PROPS
				this.carrot = ImageIO.read(new File(path + "//src//Images//Props//Extension//Axe.png")).getScaledInstance(130, 170,
						Image.SCALE_SMOOTH);
				this.lettuce = ImageIO.read(new File(path + "//src//Images//Props//Extension//Banner.png")).getScaledInstance(130, 170,
						Image.SCALE_SMOOTH);
				this.rabbit = ImageIO.read(new File(path + "//src//Images//Props//Extension//Sword.png")).getScaledInstance(130, 170,
						Image.SCALE_SMOOTH);
				this.otherRabbit = ImageIO.read(new File(path + "//src//Images//Props//Extension//Excalibur.png")).getScaledInstance(130, 170,
						Image.SCALE_SMOOTH);
				this.hat = ImageIO.read(new File(path + "//src//Images//Props//Extension//Horse.png")).getScaledInstance(130, 170,
						Image.SCALE_SMOOTH);

				//TRICKS 
				this.bunchOfCarrots = ImageIO.read(new File(path + "//src/Images/Tricks/Extension//Berserker.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);
				this.carrotHatTrick = ImageIO.read(new File(path + "//src//Images//Tricks//Extension//BoldWarrior.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);
				this.hatTrick = ImageIO.read(new File(path + "//src//Images//Tricks//Extension//LegendaryKnight.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);
				this.hungryRabbit = ImageIO.read(new File(path + "//src//Images//Tricks//Extension//InfantryMan.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);
				this.otherHatTrick = ImageIO.read(new File(path + "//src//Images//Tricks//Extension//KingArthur.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);
				this.rabbitThatDidntLikeCarrots = ImageIO.read(new File(path + "//src//Images//Tricks//Extension//RoyalGuard.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);
				this.slightlyEasierHatTrick = ImageIO.read(new File(path + "/src/Images/Tricks/Extension/Knight.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);
				this.thePairOfRabbits = ImageIO.read(new File(path + "//src//Images//Tricks//Extension//Sentinel.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);
				this.theVegetableHatTrick = ImageIO.read(new File(path + "//src//Images//Tricks//Extension//Horseman.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);
				this.vegetablePatch = ImageIO.read(new File(path + "//src//Images//Tricks//Extension//BannerCarrier.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);
				this.theRomanticKnight = ImageIO.read(new File(path + "//src//Images//Tricks//Extension//RomanticKnight.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);
				this.theKingSlayer = ImageIO.read(new File(path + "//src//Images//Tricks//Extension//KingSlayer.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);


				this.deck = ImageIO.read(new File(path + "//src//Images//deckExtension.png")).getScaledInstance(130, 170,
						Image.SCALE_SMOOTH);


				this.backBase = ImageIO.read(new File(path + "//src//Images//Props//Extension//back.png")).getScaledInstance(130, 170,
						Image.SCALE_SMOOTH);


				this.pileBack = ImageIO.read(new File(path + "//src//Images//Tricks//Extension//backPile.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);
			}
			else {

				//PROPS
				this.carrot = ImageIO.read(new File(path + "//src//Images//Props//Classic//CarrotCard.png")).getScaledInstance(130, 170,
						Image.SCALE_SMOOTH);
				this.lettuce = ImageIO.read(new File(path + "//src//Images//Props//Classic//LettuceCard.png")).getScaledInstance(130, 170,
						Image.SCALE_SMOOTH);
				this.rabbit = ImageIO.read(new File(path + "//src//Images//Props//Classic//RabbitCard.png")).getScaledInstance(130, 170,
						Image.SCALE_SMOOTH);
				this.otherRabbit = ImageIO.read(new File(path + "//src//Images//Props//Classic//OtherRabbitCard.png")).getScaledInstance(130, 170,
						Image.SCALE_SMOOTH);
				this.hat = ImageIO.read(new File(path + "//src//Images//Props//Classic//HatCard.png")).getScaledInstance(130, 170,
						Image.SCALE_SMOOTH);

				//TRICKS
				this.bunchOfCarrots = ImageIO.read(new File(path + "/src/Images/Tricks/Classic/BunchOfCarrots.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);
				this.carrotHatTrick = ImageIO.read(new File(path + "//src//Images//Tricks//Classic//CarrotHatTrick.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);
				this.hatTrick = ImageIO.read(new File(path + "//src//Images//Tricks//Classic//HatTrick.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);
				this.hungryRabbit = ImageIO.read(new File(path + "//src//Images//Tricks//Classic//HungryRabbit.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);
				this.otherHatTrick = ImageIO.read(new File(path + "//src//Images//Tricks//Classic//OtherHatTrick.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);
				this.rabbitThatDidntLikeCarrots = ImageIO.read(new File(path + "//src//Images//Tricks//Classic//RabbitThatDidntLikeCarrots.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);
				this.slightlyEasierHatTrick = ImageIO.read(new File(path + "/src/Images/Tricks/Classic/SlightyEasierHatTrick.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);
				this.thePairOfRabbits = ImageIO.read(new File(path + "//src//Images//Tricks//Classic//ThePairOfRabbits.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);
				this.theVegetableHatTrick = ImageIO.read(new File(path + "//src//Images//Tricks//Classic//TheVegetableHatTrick.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);
				this.vegetablePatch = ImageIO.read(new File(path + "//src//Images//Tricks//Classic//VegetablePatch.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);


				this.deck = ImageIO.read(new File(path + "//src//Images//deck.png")).getScaledInstance(130, 170,
						Image.SCALE_SMOOTH);


				this.backBase = ImageIO.read(new File(path + "//src//Images//Props//Classic//back.png")).getScaledInstance(130, 170,
						Image.SCALE_SMOOTH);


				this.pileBack = ImageIO.read(new File(path + "//src//Images//Tricks//Classic//backPile.png")).getScaledInstance(225, 160,
						Image.SCALE_SMOOTH);
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}


	//----------------------------------BUTTONS-------------------------------------

	/**
	 * The buttons of the extension's choice.
	 */

	public void choiceExtensionBTN() {

		btnYes = new ButtonInterface("YES");
		btnYes.setForeground(new Color(0, 0, 0));
		btnYes.setBackground(Color.LIGHT_GRAY);
		btnYes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.controlSetExtension(true);
			}
		});
		
		btnYes.setBounds(355, 357, 170, 120);
		layeredPane.add(btnYes);


		btnNo = new ButtonInterface("NO");
		btnNo.setBackground(Color.LIGHT_GRAY);
		btnNo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.controlSetExtension(false);
			}
		});
		btnNo.setBounds(702, 357, 170, 120);
		layeredPane.add(btnNo);
	}


	/**
	 * The buttons of the variants's choice.
	 */
	public void choiceVariantsBTN() {
		btnRegulargame = new ButtonInterface("Regular Game");
		btnRegulargame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.controlSetFirstVariant(false);
			}
		});
		btnRegulargame.setForeground(Color.BLACK);
		btnRegulargame.setBackground(Color.LIGHT_GRAY);
		btnRegulargame.setBounds(106, 357, 170, 120);
		layeredPane.add(btnRegulargame);


		btnPointsGame = new ButtonInterface("Points Game");
		btnPointsGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.controlSetFirstVariant(true);
			}
		});
		btnPointsGame.setForeground(Color.BLACK);
		btnPointsGame.setBackground(Color.LIGHT_GRAY);
		btnPointsGame.setBounds(389, 358, 170, 120);
		layeredPane.add(btnPointsGame);


		btnRandomPoints = new ButtonInterface("Random Points");
		btnRandomPoints.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.controleSetSecondVariant(true);
			}
		});
		btnRandomPoints.setBackground(Color.LIGHT_GRAY);
		btnRandomPoints.setBounds(672, 357, 170, 120);
		layeredPane.add(btnRandomPoints);


		btnUseTwice = new ButtonInterface("Use twice");
		btnUseTwice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.controlSetFirstVariant(true);
				controller.controleSetSecondVariant(true);
			}
		});
		btnUseTwice.setBackground(Color.LIGHT_GRAY);
		btnUseTwice.setBounds(949, 357, 170, 120);
		layeredPane.add(btnUseTwice);
	}

	/**
	 * The buttons of the number of players's choice.
	 */

	public void choiceNbrPlayersBTN() {

		buttonPlayer0 = new ButtonInterface("0");
		buttonPlayer0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.setNumberOfPlayer(game, 0);
			}
		});


		buttonPlayer1 = new ButtonInterface("1");
		buttonPlayer1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.setNumberOfPlayer(game, 1);
			}
		});
		buttonPlayer1.setForeground(Color.BLACK);
		buttonPlayer1.setBackground(Color.LIGHT_GRAY);
		buttonPlayer1.setBounds(389, 358, 170, 120);
		layeredPane.add(buttonPlayer1);


		buttonPlayer2 = new ButtonInterface("2");
		buttonPlayer2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.setNumberOfPlayer(game, 2);
			}
		});
		buttonPlayer2.setForeground(Color.BLACK);
		buttonPlayer2.setBackground(Color.LIGHT_GRAY);
		buttonPlayer2.setBounds(672, 357, 170, 120);
		layeredPane.add(buttonPlayer2);


		buttonPlayer3 = new ButtonInterface("3");
		buttonPlayer3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.setNumberOfPlayer(game, 3);
			}
		});
		buttonPlayer3.setForeground(Color.BLACK);
		buttonPlayer3.setBackground(Color.LIGHT_GRAY);
		buttonPlayer3.setBounds(949, 357, 170, 120);
		layeredPane.add(buttonPlayer3);
	}


	/**
	 * The buttons of the player name's choice.
	 */
	public void choiceNameBTN() {

		textField = new JTextField();
		layeredPane.setLayer(textField, 1);
		textField.setFont(this.ringBearer.deriveFont(13.0f));
		textField.setBounds(344, 319, 198, 39);
		layeredPane.add(textField);
		textField.setColumns(10);
		btnValidate = new ButtonInterface("validate");
		btnValidate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String result;
				if(textField.getText().length()>=2) {
					if(textField.getText().length() > 10) {
						result = textField.getText().substring(0, 10);
					} else {
						result = textField.getText();
					}
					controller.setNameOfPlayer(game, result);
					textField.setText("");
				}
			}
		});
		btnValidate.setFont(this.ringBearer.deriveFont(13.0f));
		btnValidate.setBounds(605, 319, 97, 39);
		layeredPane.add(btnValidate);
		textField.setText("");
	}

	/**
	 * The buttons of the date's choice.
	 */

	public void choiceDateBTN() {

		dayField = new JTextField();
		layeredPane.setLayer(dayField, 1);
		dayField.setHorizontalAlignment(SwingConstants.CENTER);
		dayField.setText("11");
		dayField.setBounds(470, 295, 40, 40);
		layeredPane.add(dayField);
		dayField.setColumns(2);

		monthField = new JTextField();
		layeredPane.setLayer(monthField, 1);
		monthField.setText("11");
		monthField.setHorizontalAlignment(SwingConstants.CENTER);
		monthField.setColumns(2);
		monthField.setBounds(580, 295, 40, 40);
		layeredPane.add(monthField);

		yearField = new JTextField();
		layeredPane.setLayer(yearField, 1);
		yearField.setText("2011");
		yearField.setHorizontalAlignment(SwingConstants.CENTER);
		yearField.setColumns(4);
		yearField.setBounds(685, 295, 72, 40);
		layeredPane.add(yearField);

		dateLabel = new JLabel("/                    /");
		dateLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		dateLabel.setBounds(545, 305, 152, 16);
		layeredPane.add(dateLabel);

		validateDate = new ButtonInterface("validate");
		validateDate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int day = Integer.parseInt(dayField.getText());
					int month = Integer.parseInt(monthField.getText());
					int year = Integer.parseInt(yearField.getText());
					if(day>0 && day <= 31 && month >0 && month <= 12 && year>1850 && year <2019) {
						controller.controlerSetDate(game, day, month, year);
						dayField.setText("11");
						monthField.setText("11");
						yearField.setText("2011");

					} 
					else {
						errorText.setVisible(true);
						errorText.setText("Please write a real date");
					}
				} 
				catch (NumberFormatException i) {
					errorText.setVisible(true);
					errorText.setText("Please write a real date");
				}
			}
		});
		validateDate.setBounds(808, 295, 97, 39);
		validateDate.setFont(this.ringBearer.deriveFont(13.0f));
		layeredPane.add(validateDate);
	}

	/**
	 * The buttons of the strategies's choice.
	 */

	public void choiceStrategyBTN() {

		btnIntelligent = new ButtonInterface("Intelligent");
		btnIntelligent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.controlSetStrategy(game, 1);
			}
		});
		btnIntelligent.setForeground(Color.BLACK);
		btnIntelligent.setBackground(Color.LIGHT_GRAY);
		btnIntelligent.setBounds(355, 357, 170, 120);
		layeredPane.add(btnIntelligent);


		btnRandom = new ButtonInterface("Random");
		btnRandom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.controlSetStrategy(game, 2);
			}
		});
		btnRandom.setBackground(Color.LIGHT_GRAY);
		btnRandom.setBounds(702, 357, 170, 120);
		layeredPane.add(btnRandom);
	}

	/**
	 * The button of the trick pile.
	 */

	public void trickPileBTN() {

		lblIngeance = new JLabel("Ingeance");
		lblIngeance.setForeground(Color.WHITE);
		lblIngeance.setBackground(Color.WHITE);
		lblIngeance.setFont(this.ringBearer.deriveFont(35.0f));
		lblIngeance.setBounds(1081, 598, 181, 75);
		layeredPane.add(lblIngeance);


		imgTrickPile = new ButtonInterface("");
		layeredPane.setLayer(imgTrickPile, 0);
		imgTrickPile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(state == "ChooseToPerform") {
					controller.controlerSetChoosePerform(game.getPlayers()[game.getPlayerIndex()], 1);

					state ="";
				}
			}
		});
		imgTrickPile.setBounds(42, 371, 224, 160);
		layeredPane.add(imgTrickPile);
	}


	/**
	 * The button of the trick deck.
	 */
	public void trickDeckBTN() {

		imgTrickDeck = new ButtonInterface("");
		imgTrickDeck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(state == "ChooseToPerform") {
					controller.controlerSetChoosePerform(game.getPlayers()[game.getPlayerIndex()], 2);

					state ="";
				}
			}
		});
		imgTrickDeck.setBounds(288, 361, 121, 170);
		layeredPane.add(imgTrickDeck);
	}


	/**
	 * The buttons of the actual player.
	 */
	public void playerCardsBTN() {

		leftCardJ1 = new ButtonInterface("leftCardJ1");
		leftCardJ1.setBounds(421, 490, 121, 170);
		layeredPane.add(leftCardJ1);
		leftCardJ1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(state == "ChooseMyCard") {
					controller.controlerSetChoice(game.getPlayers()[game.getPlayerIndex()], 1);

					state = "";
				} 
				else if(state == "Return Card") {
					controller.controlerSetChoiceCardReturn(game.getPlayers()[game.getPlayerIndex()], 1);

					state = "";

				}
				else if (state == "ChooseSeventhProp") {
					controller.controlerSetSeventhPropChoice(game.getPlayers()[game.getPlayerIndex()], 1);

					state = "";
				}
			}
		});


		rightCardJ1 = new ButtonInterface("rightCardJ1");
		rightCardJ1.setBounds(619, 490, 121, 170);
		layeredPane.add(rightCardJ1);
		rightCardJ1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(state == "ChooseMyCard") {
					controller.controlerSetChoice(game.getPlayers()[game.getPlayerIndex()], 2);

					state = "";
				}
				else if(state == "Return Card") {
					controller.controlerSetChoiceCardReturn(game.getPlayers()[game.getPlayerIndex()], 2);

					state = "";
				}
				else if (state == "ChooseSeventhProp") {
					controller.controlerSetSeventhPropChoice(game.getPlayers()[game.getPlayerIndex()], 2);

					state = "";
				}
			}
		});


		btnSeventhProp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (state == "ChooseSeventhProp")
				{
					controller.controlerSetSeventhPropChoice(game.getPlayers()[game.getPlayerIndex()], 3);

					state = "";
				}
			}
		});
	}



	public void firstVariantPointsBTN() {

		firstVariantPointsField = new JTextField();
		firstVariantPointsField.setHorizontalAlignment(SwingConstants.CENTER);
		firstVariantPointsField.setText("50");
		layeredPane.setLayer(firstVariantPointsField, 1);
		firstVariantPointsField.setBounds(580, 296, 40, 40);
		layeredPane.add(firstVariantPointsField);
		firstVariantPointsField.setColumns(10);
		firstVariantPointsField.setFont(this.ringBearer.deriveFont(13.0f));
		validateFirstVariantPoints = new ButtonInterface("validate");
		validateFirstVariantPoints.setFont(this.ringBearer.deriveFont(13.0f));
		validateFirstVariantPoints.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int choice = 0;
				try {
					choice =  Integer.parseInt(firstVariantPointsField.getText());

				} catch(NumberFormatException e) {
					errorText.setVisible(true);
					errorText.setText("Please write a number");
				}
				if(choice > 0 && choice <=50) {
					controller.controleSetFirstVariantPoints(choice);
				}
			}
		});
		validateFirstVariantPoints.setBounds(550, 357, 97, 25);
		layeredPane.add(validateFirstVariantPoints);
	}


	/**
	 * The buttons of the other players.
	 */
	public void otherPlayersCards() {

		leftCardJ2 = new ButtonInterface("leftCardJ2");
		leftCardJ2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(state == "ChooseOpponentCard") {
					controller.controlerSetChooseTheOtherCard(game.getPlayers()[game.getPlayerIndex()], 1);

					state ="";
				}
			}
		});
		leftCardJ2.setBounds(879, 68, 121, 170);
		layeredPane.add(leftCardJ2);

		rightCardJ2 = new ButtonInterface("rightCardJ2");
		rightCardJ2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(state == "ChooseOpponentCard") {
					controller.controlerSetChooseTheOtherCard(game.getPlayers()[game.getPlayerIndex()], 2);

					state ="";
				}
			}
		});
		rightCardJ2.setBounds(1034, 70, 121, 170);
		layeredPane.add(rightCardJ2);

		leftCardJ3 = new ButtonInterface("leftCardJ3");
		leftCardJ3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(state == "ChooseOpponentCard") {
					controller.controlerSetChooseTheOtherCard(game.getPlayers()[game.getPlayerIndex()], 3);

					state ="";
				}
			}
		});
		leftCardJ3.setBounds(56, 68, 121, 170);
		layeredPane.add(leftCardJ3);

		rightCardJ3 = new ButtonInterface("rightCardJ3");
		rightCardJ3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(state == "ChooseOpponentCard") {
					controller.controlerSetChooseTheOtherCard(game.getPlayers()[game.getPlayerIndex()], 4);

					state ="";
				}
			}
		});
		rightCardJ3.setBounds(222, 68, 121, 170);
		layeredPane.add(rightCardJ3);
	}

	//-------------------------------------DESACTIVATE AND ACTIVATE BTN----------------------------------------
	/**
	 * The operation to desactive the buttons of the extension's choice.
	 */
	public void desactiveChoiceExtensionBTN() {

		this.btnNo.setVisible(false);
		this.btnYes.setVisible(false);
		this.errorText.setVisible(false);
	}

	/**
	 * The operation to desactive the buttons of the name's choice.
	 */

	public void desactiveChoiceNameBTN() {

		this.textField.setVisible(false);
		this.btnValidate.setVisible(false);
		this.errorText.setVisible(false);
	}

	/**
	 * The operation to active the buttons of the name's choice.
	 */

	public void activeChoiceNameBTN() {

		this.textField.setVisible(true);
		this.btnValidate.setVisible(true);
	}

	/**
	 * The operation to desactive the buttons of the strategies's choice.
	 */

	public void desactiveChoiceStrategyBTN() {

		this.btnIntelligent.setVisible(false);
		this.btnRandom.setVisible(false);
		this.errorText.setVisible(false);
	}
	/**
	 * The operation to active the buttons of the strategies's choice.
	 */


	public void activeChoiceStrategyBTN() {

		this.btnIntelligent.setVisible(true);
		this.btnRandom.setVisible(true);
		this.errorText.setVisible(false);
	}

	/**
	 * The operation to desactive the buttons of the number of players's choice.
	 */

	public void desactiveChoiceNbrPlayersBTN() {

		this.buttonPlayer0.setVisible(false);
		this.buttonPlayer1.setVisible(false);
		this.buttonPlayer2.setVisible(false);
		this.buttonPlayer3.setVisible(false);
	}
	/**
	 * The operation to active the buttons of the number of players's choice.
	 */


	public void activeChoiceNbrPlayersBTN() {

		this.buttonPlayer0.setVisible(true);
		this.buttonPlayer1.setVisible(true);
		this.buttonPlayer2.setVisible(true);
		this.buttonPlayer3.setVisible(true);
	}

	/**
	 * The operation to desactive the buttons of the variants's choice.
	 */

	public void desactiveChoiceVariantsBTN() {

		this.btnPointsGame.setVisible(false);
		this.btnRandomPoints.setVisible(false);
		this.btnRegulargame.setVisible(false);
		this.btnUseTwice.setVisible(false);
	}

	/**
	 * The operation to active the buttons of the variants's choice.
	 */
	
	public void activeChoiceVariantsBTN(){

		this.btnPointsGame.setVisible(true);
		this.btnRandomPoints.setVisible(true);
		this.btnRegulargame.setVisible(true);
		this.btnUseTwice.setVisible(true);
	}


	/**
	 * The operation to desactive the buttons of the date's choice.
	 */
	public void desactiveChoiceDateBTN() {

		this.dateLabel.setVisible(false);
		this.dayField.setVisible(false);
		this.monthField.setVisible(false);
		this.yearField.setVisible(false);
		this.validateDate.setVisible(false);
		this.errorText.setVisible(false);
	}

	/**
	 * The operation to active the buttons of the date's choice.
	 */

	public void activeChoiceDateBTN() {

		this.dateLabel.setVisible(true);
		this.dayField.setVisible(true);
		this.monthField.setVisible(true);
		this.yearField.setVisible(true);
		this.validateDate.setVisible(true);
	}


	/**
	 * The operation to desactive the game's buttons.
	 */
	
	public void desactiveGameBTN() {

		this.lblPlayersTurn.setVisible(false);
		this.lblTrickdeck.setVisible(false);
		this.lblTrickpile.setVisible(false);
		this.imgTrickPile.setVisible(false);
		this.imgTrickDeck.setVisible(false);
		this.btnSeventhProp.setVisible(false);
		this.leftCardJ1.setVisible(false);
		this.rightCardJ1.setVisible(false);
		this.leftCardJ2.setVisible(false);
		this.rightCardJ2.setVisible(false);
		this.leftCardJ3.setVisible(false);
		this.rightCardJ3.setVisible(false);
		this.showSuccessNo.setVisible(false);
		this.showSuccessYes.setVisible(false);
		this.trickPointLbl.setVisible(false);
		this.errorText.setVisible(false);
	}

	/**
	 * The operation to active the game's buttons.
	 */
	

	public void activeGameBTN() {

		this.lblPlayersTurn.setVisible(true);
		this.lblTrickdeck.setVisible(true);
		this.lblTrickpile.setVisible(true);
		this.imgTrickPile.setVisible(true);
		this.imgTrickDeck.setVisible(true);
		this.btnSeventhProp.setVisible(true);
		this.leftCardJ1.setVisible(true);
		this.rightCardJ1.setVisible(true);
		this.leftCardJ2.setVisible(true);
		this.rightCardJ2.setVisible(true);
		this.leftCardJ3.setVisible(true);
		this.rightCardJ3.setVisible(true);
		this.trickPointLbl.setVisible(true);
	}


	/**
	 * The operation to desactive the buttons of first variant's choice.
	 */
	
	public void desactiveFirstVariantPoints() {

		this.firstVariantPointsField.setVisible(false);
		this.validateFirstVariantPoints.setVisible(false);
		this.errorText.setVisible(false);
	}
	/**
	 * The operation to active the buttons of first variant's choice.
	 */
	public void activeFirstVariantPoints() {

		this.firstVariantPointsField.setVisible(true);
		this.validateFirstVariantPoints.setVisible(true);
	}

	/**
	 * Create the game's font from TFF file.
	 */

	public void loadFont() {

		try {
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();

			File fontFile = new File(path + "/src/Fonts/RingBearerFont.TTF");

			this.ringBearer = Font.createFont(Font.TRUETYPE_FONT, fontFile);
			ge.registerFont(ringBearer);

		}
		catch (IOException|FontFormatException e) {
			//Handle exception
			e.printStackTrace();
		}
	}
}
