package vue;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 * The class which will create a window with a image in background.
 * @author Ingeance
 *
 */
public class JPanelWithBackground extends JPanel {

 /**
  * The ID of the serialized class.
  */
private static final long serialVersionUID = 1L;

/**
 * The image in backgroud.
 */
private Image backgroundImage;

 /**
  * The constructor of the background image.
  * @param fileName {String} : the adress of the image in the computer.
  */
  public JPanelWithBackground(String fileName) {
    try {
		backgroundImage = ImageIO.read(new File(fileName));
	} catch (IOException e) {
		
		e.printStackTrace();
	}
  }
  
  /**
   * Draw the background image.
   * @param g {Graphics} The graphics of the graphic view.
   */
  public void paintComponent(Graphics g) {
    super.paintComponent(g);

    g.drawImage(this.backgroundImage, 0, 0, this);
  }
}