package vue;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

/**
 * Buttons displayed in the Graphic Interface.
 * Buttons reacts when the mouse is over or when they are pressed.
 * @author Ingeance
 *
 */
public class ButtonInterface extends JButton implements MouseListener {


	private static final long serialVersionUID = -8810547252122208784L;
	Border buttonBorder;
	public ButtonInterface(String arg0) {
		super(arg0);
		this.buttonBorder = new LineBorder(Color.LIGHT_GRAY, 3);
		this.setBackground(Color.LIGHT_GRAY);
		this.setOpaque(true);
		this.setBorder(this.buttonBorder);
		this.addMouseListener(this);
	
	}

	/**
	 * When we click.
	 * @param arg0 MouseEvent
	 */
	public void mouseClicked(MouseEvent arg0) {
	
		
	}

	/**
	 * When the mouse is over the button, the border become white.
	 * @param arg0 MouseEvent
	 */
	public void mouseEntered(MouseEvent arg0) {
	
		this.buttonBorder = new LineBorder(Color.WHITE, 3);
		this.setBorder(this.buttonBorder);
		
	}

	/**
	 * When the mouse leaves the button, the border become light gray.
	 * @param arg0 MouseEvent
	 */
	public void mouseExited(MouseEvent arg0) {
		this.buttonBorder = new LineBorder(Color.LIGHT_GRAY, 3);
		this.setBorder(this.buttonBorder);
		
	}

	/**
	 * When we press the boutton, the border become red.
	 * @param arg0 MouseEvent
	 */
	public void mousePressed(MouseEvent arg0) {
	
		this.buttonBorder = new LineBorder(Color.red, 3);
		this.setBorder(this.buttonBorder);
	}

	/**
	 * When the click is ended, the border become light gray.
	 * @param arg0 MouseEvent
	 */
	public void mouseReleased(MouseEvent arg0) {
		this.buttonBorder = new LineBorder(Color.LIGHT_GRAY, 3);
		this.setBorder(this.buttonBorder);
		
	}




}
