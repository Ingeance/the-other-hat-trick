package vue;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

/**
 * This class allows the player to continuously write someting in the console and avoid the operations to be paused by the scanner.
 * @author Ingeance
 *
 */


public class ConsoleInput extends Observable implements Runnable{
	
	/**
	 * The thread used by the console input.
	 */
	private Thread thread;
	/**
	 * The boolean to check if the console input is running.
	 */
	private boolean isRunning;

	/**
	 * Constructor of the console input
	 * @param obs {Observer} The console's view which will observes this.
	 */
	public ConsoleInput(Observer obs) {
		this.addObserver(obs);
		thread = new Thread(this);
	}
	
	/**
	 * Launch the thread and the input.
	 */
	public void startInput() {
		this.isRunning = true;
		thread.start();
	}
	
	/**
	 * Stop the input and the thread.
	 */
	public void stopInput() {
		this.isRunning = false;
	}

	/**
	 * Run method which implements Observable.
	 * Create a scanner which will notify every line entered in the console to the console's view.
	 */
	public void run() {
		Scanner input = new Scanner(System.in);
		
		while(isRunning) {
			String line = input.nextLine();
			
	
			setChanged();
			notifyObservers(line);
		}
	
		input.close();
	}
	
	

}