package vue;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

import controllerPackage.Controller;
import theOtherHatTrick.Events;
import theOtherHatTrick.Game;
import theOtherHatTrick.Player;
import theOtherHatTrick.Prop;
import theOtherHatTrick.TrickCard;
import theOtherHatTrick.TrickDeck;
import theOtherHatTrick.TrickPile;
import theOtherHatTrick.VirtualPlayer;

/**
 * The interactive console's view of the game.
 * @author Ingeance
 *
 */
@SuppressWarnings("deprecation")
public class TheOtherHatTrickConsole implements Observer {

	public static String QUITTER = "Quit";
	public static String PROMPT = "> ";
	private Game game;
	private Controller controller;
	private Thread consoleViewThread;
	private ConsoleInput consoleInput;

	/**
	 * The constructor of the console's view.
	 * @param g {Game} : the actual game playing.
	 * @param c {Controleur} : the actual controleur of the game.
	 */
	public TheOtherHatTrickConsole(Game g, Controller c) {

		this.game = g;
		this.game.addObserver(this);
		this.controller = c;
		this.consoleInput = new ConsoleInput(c);
		this.consoleInput.startInput();


	}


	/*
	 * Update operation of the Observer.
	 * The view will react to all the events according to the MVC's design pattern.
	 * @param o {Observable} : An object which implements Observable.
	 * @param arg {Object} : The argument sent in the notification of the Observable. It's the enumeration Evenements.
	 */


	@Override
	public void update(Observable o, Object arg) {

		if (o instanceof Game) {

			switch (((Events)arg)) {
			case Welcoming:
				System.out.println("******************** Welcome to The Other Hat Trick ********************");
				System.out.println("________________________________________________________________________\n");
				break;

			case ExtensionChoice:
				this.consoleViewThread= new Thread(new Runnable() {
					public void run() {

						int extensionID = 0;

						System.out.print("* Do you want to use Middle-Age extension ? It changes the universe of the game and add two new trick cards !*\n 1. Yes\n 2. No\n" + TheOtherHatTrickConsole.PROMPT);

						while(extensionID != 1 && extensionID !=2 && controller.getLastInput() != "break") {
							extensionID = askInt();
						}


						if(extensionID == 1) {
							controller.controlSetExtension(true);
						}
						else if(extensionID == 2) {
							controller.controlSetExtension(false);
						}
					}
				});
				consoleViewThread.start();
				break;

			case VariantsChoice:
				this.consoleViewThread= new Thread(new Runnable() {
					public void run() {
						int variantID = 0;
						controller.setLastInput("0");
						System.out.print("* Do you want to use variants ? *\n 1. Regular game. \n 2. The winner is defined by his number of points, can be used for short or very long games !\n 3. The points of the tricks are given randomly !\n 4. Active the two variants\n" + TheOtherHatTrickConsole.PROMPT);
						while(variantID != 1 && variantID != 2 && variantID !=3 && variantID !=4 && controller.getLastInput() != "break") {
							variantID = askInt();
						}


						if(variantID == 1) {
							controller.controlSetFirstVariant(false);
						}
						if(variantID == 2 || variantID == 4) controller.controlSetFirstVariant(true);
						if(variantID == 3 || variantID == 4) controller.controleSetSecondVariant(true);
					}		
				});
				consoleViewThread.start();
				break;

			case FirstVariantChoice :
				this.consoleViewThread= new Thread(new Runnable() {
					public void run() {
						int choiceFirstVariant = 0;
						controller.setLastInput("0");
						System.out.println("How many points will be necessary ? (Maximum 50)");

						while(choiceFirstVariant < 1 || choiceFirstVariant>50) {
							if(controller.getLastInput() == "break") break;
							choiceFirstVariant = askInt();
						}

						if(controller.getLastInput() != "break") {
							controller.controleSetFirstVariantPoints(choiceFirstVariant);
						}
					}
				});
				consoleViewThread.start();
				break;

			case NumberOfPlayers:
				this.consoleViewThread= new Thread(new Runnable() {
					public void run() {
						int numberOfPlayers = -1;
						controller.setLastInput("-1");
						System.out.print("* Please enter the numbers of real players (max 3) *\n" + TheOtherHatTrickConsole.PROMPT);
						while((numberOfPlayers < 0 || numberOfPlayers > 3)) {
							if(controller.getLastInput() == "break") break;
							numberOfPlayers = askInt();
						}
						if(controller.getLastInput() != "break") {
							controller.setNumberOfPlayer(game, numberOfPlayers);
						}
					}
				});
				consoleViewThread.start();
				break;

			case Equality:
				System.out.println("Equality !");

				break;
			case DisplayWinner: 
				System.out.println("The winner is : " + this.game.getWinner().getName()+ " with " + this.game.getWinner().getNumberOfPoint() + " points");
				break;

			case DisplayTwoWinners:
				System.out.println("The winner is : " + this.game.getWinner().getName()+ " with " + this.game.getWinner().getNumberOfPoint() + " points");
				System.out.println("The second winner is : " + this.game.getWinner2().getName() + " with " + this.game.getWinner2().getNumberOfPoint() + " points");
				break;

			case ChooseName:
				this.consoleViewThread= new Thread(new Runnable() {
					public void run() {
						try {
							Thread.currentThread().sleep(100);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						System.out.print("Player " + (game.getCounter()+1) + ",\nPlease write your name, \nOnly 10 first letters will be counted, 2 letters minimum.\n" + TheOtherHatTrickConsole.PROMPT);
						controller.setLastInput("0");
						String name = "0";
						while(name.length() < 2) {
							name = "" + controller.getLastInput();
						}
						if(name.length()>10) {
							name = name.substring(0, 10);
						}
						if(controller.getLastInput() != "break") {
							controller.setNameOfPlayer(game, name);
						}
					}
				});
				consoleViewThread.start();
				break;

			case ChooseStrategy:
				this.consoleViewThread= new Thread(new Runnable() {
					public void run() {
						int choice = -1;
						controller.setLastInput("-1");

						System.out.print("Please choose the strategy to adopt for " + game.getName() + " :\n 1. Intelligent  \n 2. Dumb (all random) \n" + TheOtherHatTrickConsole.PROMPT);
						while(choice != 1 && choice != 2 && controller.getLastInput() != "break") {
							choice = askInt();
						}
						if(controller.getLastInput() != "break") {
							controller.controlSetStrategy(game, choice);
						}
					}
				});
				consoleViewThread.start();
				break;

			case Youngest:
				System.out.println("___________________________________________________________\n");
				System.out.println("The youngest player is " + (this.game.getPlayers()[this.game.getYoungestPlayer()].getName()) + ". It's your turn !");
				break;

			case AddObervers:
				Player[] players = this.game.getPlayers();
				for (int i = 0; i < players.length; i++)
					players[i].addObserver(this);

				LinkedList<Prop> propCards = this.game.getPropCards();
				Iterator<Prop> itProp = propCards.iterator();
				while (itProp.hasNext()) {
					Prop propCard = itProp.next();
					propCard.addObserver(this);
				}

				TrickDeck trickDeckObs = this.game.getTrickDeck();
				trickDeckObs.addObserver(this);

				Iterator<TrickCard> itTrick = trickDeckObs.getListOfTricks().iterator();
				while (itTrick.hasNext()) {
					TrickCard trickCard = itTrick.next();
					trickCard.addObserver(this);
				}

				TrickPile trickPileObs = this.game.getTrickDeck().getTrickPile();
				trickPileObs.addObserver(this);
				break;

			case PlayerTurn: 
				System.out.println("\nIt's " + (this.game.getPlayers()[this.game.getPlayerIndex()].getName()) + "'s turn !");
				break;

			case BarSeparation: 
				System.out.println("___________________________________________________________\n");
				break;

			case PlayerCards: 
				System.out.println("* You have " + game.getPlayers()[game.getPlayerIndex()].getLeftCard().toString() + " and " + game.getPlayers()[game.getPlayerIndex()].getRightCard().toString() + " *");		
				break;

			case PileDeckCards:
				System.out.println("\nThere are " + this.game.getTrickDeck().getNumberOfCardRemainging() + " card(s) in the deck"); 
				System.out.println("There are " + this.game.geTrickPile().getNumberOfCards() + " card(s) in the pile");
				break;

			case Date :
				this.consoleViewThread= new Thread(new Runnable() {
					public void run() {
						int day = 0,month = 0,year = 0;
						controller.setLastInput("0");
						System.out.print("* Please enter your day of birth : *\n" + TheOtherHatTrickConsole.PROMPT);
						while((day < 1 || day > 31 )&& controller.getLastInput() != "break") {
							day = askInt();

						}
						if(controller.getLastInput() != "break") controller.setLastInput("0");
						System.out.print("* Please enter your month of birth : *\n" + TheOtherHatTrickConsole.PROMPT);
						while((month < 1 || month > 12)&& controller.getLastInput() != "break") {

							month = askInt();

						}
						if(controller.getLastInput() != "break") controller.setLastInput("0");
						System.out.print("* Please enter your year of birth : *\n" + TheOtherHatTrickConsole.PROMPT);
						while((year < 1850 || year > 2018)&& controller.getLastInput() != "break") {
							year = askInt();
						}
						if(controller.getLastInput() != "break") {
							controller.controlerSetDate(game, day, month, year);
						}
					}
				});
				consoleViewThread.start();
				break;

			default:
				System.out.println("/!\\Cet etat n'est pas repertorié /!\\");
				break;

			}
		}
		else if (o instanceof Player) {

			switch (((Events)arg)) {
			case ChooseCard:
				this.consoleViewThread= new Thread(new Runnable() {
					public void run() {
						int choice = 0;
						controller.setLastInput("0");
						System.out.print("Please choose between your card 1/ " + game.getPlayers()[game.getPlayerIndex()].getLeftCard() + " and your card 2/ " + game.getPlayers()[game.getPlayerIndex()].getRightCard() +"\n" + TheOtherHatTrickConsole.PROMPT);
						while (choice != 1 && choice != 2&& controller.getLastInput() != "break") {
							choice = askInt();
						}
						if(controller.getLastInput() != "break") {
							controller.controlerSetChoice(game.getPlayers()[game.getPlayerIndex()], choice);
						}
					}
				});
				consoleViewThread.start();
				break;

			case DisplayPlayersCards:
				String displayPlayersCards ="";
				displayPlayersCards = this.game.getPlayers()[this.game.getPlayerIndex()].getDisplayCards();
				System.out.println(displayPlayersCards);
				break;

			case ChooseProposedCard: 
				this.consoleViewThread= new Thread(new Runnable() {
					public void run() {
						int chooseTheOtherCard = 0;
						controller.setLastInput("0");
						System.out.print("Please choose the card of the player whith whom you want to trade  : \n" + TheOtherHatTrickConsole.PROMPT);
						while (chooseTheOtherCard != 1 && chooseTheOtherCard != 2 && chooseTheOtherCard != 3 && chooseTheOtherCard != 4&& controller.getLastInput() != "break") {
							chooseTheOtherCard = askInt();
						}
						if(controller.getLastInput() != "break") {
							controller.controlerSetChooseTheOtherCard(game.getPlayers()[game.getPlayerIndex()], chooseTheOtherCard);
						}
					}
				});
				consoleViewThread.start();
				break;

			case ChooseToPerform :
				this.consoleViewThread= new Thread(new Runnable() {
					public void run() {
						int choicePerform = 0;
						controller.setLastInput("0");
						System.out.print("1. Do you want to perform this trick ? : " + game.getTrickDeck().getTrickPile().getTopCardTrickPile() + "\n2. Or perfom a mysterious trick ?\n" + TheOtherHatTrickConsole.PROMPT);
						while (choicePerform !=1 && choicePerform !=2 && controller.getLastInput() != "break" ) {
							choicePerform = askInt();
						}

						if(controller.getLastInput() != "break") {
							controller.controlerSetChoosePerform(game.getPlayers()[game.getPlayerIndex()], choicePerform);
						}
					}
				});
				consoleViewThread.start();
				break;

			case DisplayLastTrick :
				System.out.println("There are no more cards available, the trick is : " + this.game.getTrickDeck().getTrickPile().getTopCardTrickPile());
				break;

			case DisplayTopPile :
				System.out.println(this.game.getTrickDeck().getTrickPile().getTopCardTrickPile());
				break;

			case ShowSucess :
				this.consoleViewThread= new Thread(new Runnable() {
					public void run() {
						int choiceShow = 0;
						controller.setLastInput("0");
						System.out.print("Do you want to show that you have succeeded the trick ? Yes = 1, No = 2\n" + TheOtherHatTrickConsole.PROMPT);
						while (choiceShow != 1 && choiceShow != 2 && controller.getLastInput() != "break") {
							choiceShow = askInt();
						}
						if(controller.getLastInput() != "break") {
							controller.controlerSetShow(game.getPlayers()[game.getPlayerIndex()], choiceShow);
						}
					}
				});
				consoleViewThread.start();
				break;

			case ShowPoints :
				System.out.println("You have " + this.game.getPlayers()[this.game.getPlayerIndex()].getNumberOfPoint() + " points !");
				break;

			case ExchangeSeventhProp:
				this.consoleViewThread= new Thread(new Runnable() {
					public void run() {
						Player player7 = game.getPlayers()[game.getPlayerIndex()];


						System.out.println("The seventh card is : " + game.getSeventhProp());

						int seventhProp = 0;
						controller.setLastInput("0");
						System.out.print("Which card do you want to put in the middle ? : 1/ " + player7.getLeftCard() + " 2/ " + player7.getRightCard() + " 3/ " + game.getSeventhProp() + "\n" + TheOtherHatTrickConsole.PROMPT);
						while (seventhProp != 1 && seventhProp != 2 && seventhProp != 3&&controller.getLastInput() != "break") {
							seventhProp = askInt();
						}
						if(controller.getLastInput() != "break") {
							controller.controlerSetSeventhPropChoice(player7, seventhProp);
						}
					}
				});
				consoleViewThread.start();
				break;

			case TrickFailure :
				System.out.println("You failed your trick ...");
				break;

			case ReturnCard :
				this.consoleViewThread= new Thread(new Runnable() {
					public void run() {
						Player player9 = game.getPlayers()[game.getPlayerIndex()];

						int choiceCardReturn = 0;
						/** Verification loop */
						controller.setLastInput("0");
						System.out.print("Which card do you want to return ? 1/ " + player9.getLeftCard() + " 2/ " + player9.getRightCard() + "\n" + TheOtherHatTrickConsole.PROMPT);
						while (choiceCardReturn != 1 && choiceCardReturn != 2&&controller.getLastInput() != "break") {
							choiceCardReturn = askInt();
						}
						if(controller.getLastInput() != "break") {
							controller.controlerSetChoiceCardReturn(player9, choiceCardReturn);
						}
					}
				});
				consoleViewThread.start();
				break;

			case CardsReturned:
				System.out.println("No more malus, your cards are return");
				break;

			case DisplayTrickHasToPerform:
				System.out.println((((VirtualPlayer) this.game.getPlayers()[this.game.getPlayerIndex()]).getTrickPile().getTopCardTrickPile()));
				break;

			case VirtualPlayerShowSucess:
				System.out.println((((VirtualPlayer) this.game.getPlayers()[this.game.getPlayerIndex()]).getName()) + " performed " + (((VirtualPlayer) this.game.getPlayers()[this.game.getPlayerIndex()]).getTrickHasToPerform().getName()) + " with the cards " + (((VirtualPlayer) this.game.getPlayers()[this.game.getPlayerIndex()]).getLeftCard()) + " and " + (((VirtualPlayer) this.game.getPlayers()[this.game.getPlayerIndex()]).getRightCard()));
				System.out.println((((VirtualPlayer) this.game.getPlayers()[this.game.getPlayerIndex()]).getName()) + " has " + (((VirtualPlayer) this.game.getPlayers()[this.game.getPlayerIndex()]).getNumberOfPoint()) + " points !");
				break;

			case VirtualPlayerShowFail:
				System.out.println((((VirtualPlayer) this.game.getPlayers()[this.game.getPlayerIndex()]).getName()) + "  failed to perform  " + (((VirtualPlayer) this.game.getPlayers()[this.game.getPlayerIndex()]).getTrickHasToPerform().getName()) + " with the cards " + (((VirtualPlayer) this.game.getPlayers()[this.game.getPlayerIndex()]).getLeftCard()) + " and " + (((VirtualPlayer) this.game.getPlayers()[this.game.getPlayerIndex()]).getRightCard()));
				break;

			default:
				break;

			}
		}


	}

	/**
	 * Method use to get integer number from the last sentence entered in the Console Input.
	 * @return The desired integer
	 */
	public int askInt() {

		try {
			int result = Integer.parseInt(this.controller.getLastInput());
			return result;
		} catch (NumberFormatException e) {
			return -1;
		}
	}



}
